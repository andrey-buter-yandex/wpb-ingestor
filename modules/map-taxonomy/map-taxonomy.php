<?php
/**
* Attach imported post to specified terms
*
* @todo 
*/

class RSSI_Module_Taxonomy_Map {

	function __construct() {

		add_action( 'rssi_import_campaign', array( &$this, 'map_terms' ) );

		// Add checked campign terms to default postarr
		// add_filter( 'rssi_import_post_default', array( &$this, 'attach_terms' ), 10, 2 );

		// add_action( 'rssi_import_post', array( &$this, 'debug_terms_data', 10, 2 ) );

		// Include meta handler class
		require( RSSI_ABSPATH . '/modules/map-taxonomy/meta.php' );
	}

	public function map_terms( $campaign_id ) {
		if ( $terms = get_campaign_meta( $campaign_id, 'terms', true ) ) {
			foreach ($terms as $taxonomy => $tax_terms) {
				$terms[ $taxonomy ] = array_map('absint', $tax_terms);
			}

			// Store available terms
			wp_cache_set( 'rssi-campaign-terms', $terms );

			add_action( 'rssi_import_post', array( &$this, 'wp_set_object_terms' ) );
		} else {
			wp_cache_delete( 'rssi-campaign-terms' );
		}

	}

	public function wp_set_object_terms( $post_id ) {
		if ( $taxonomies = wp_cache_get( 'rssi-campaign-terms' ) ) {
			foreach ($taxonomies as $taxonomy => $terms) {
				wp_set_post_terms( $post_id, $terms, $taxonomy );
			}
		}
	}

}

/**
 * Init module after plugin core loaded
 */
add_action( 'rssi_after_setup', 'init_rssi_module_taxonomy_map' );
function init_rssi_module_taxonomy_map() {
	new RSSI_Module_Taxonomy_Map;
}