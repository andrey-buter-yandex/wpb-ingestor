<?php 
global $post;
$checked = get_campaign_meta( $post->ID, 'terms', true );
$checked = is_array( $checked ) ? $checked : array();

$post_type = get_campaign_meta( $post->ID, 'post_type', true );
$post_type = empty( $post_type ) ? 'post' : $post_type;
$taxononomies = get_object_taxonomies( $post_type, 'objects' );
?>
<div class="rss-import">

	<!-- Loop through all public taxononomies -->
	<?php foreach ( $taxononomies as $taxonomy ):
		$terms = get_terms( $taxonomy->name, array( 'hide_empty' => false ) );
		if ( empty( $terms ) )
			continue; 
		$checked_terms = array_key_exists( $taxonomy->name , $checked ) ? $checked[ $taxonomy->name ] : array() ;
		?>
		<div class="checkboxes term-checkboxes">
			<h4 class="checkbox-title"><?php echo $taxonomy->labels->name; ?> :</h4>
			<?php foreach ( $terms as $term ): ?>
				<label for="rssi-<?php echo $term->taxonomy ?>-<?php echo $term->term_id; ?>">
					<input 
						type="checkbox" 
						name="rssi[terms][<?php echo $term->taxonomy ?>][]" 
						value="<?php echo $term->term_id ?>" 
						id="rssi-<?php echo $term->taxonomy ?>-<?php echo $term->term_id ?>" 
						<?php checked( true, in_array( $term->term_id , $checked_terms ) ); ?> />
					<?php echo $term->name; ?>
				</label>
			<?php endforeach ?>
		</div>
	<?php endforeach ?>

</div>