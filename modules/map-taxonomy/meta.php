<?php
/**
* Handle taxonomy map meta inside campaign
*
* @todo 
*/

class RSSI_Module_Taxonomy_Map_Meta {

	function __construct() {
		if ( is_admin() ) {
			add_action( 'init', array( &$this, 'init_meta_box' ) );
		}
	}

	public function init_meta_box() {
		global $pagenow;

		if ( !(  ( defined('DOING_AJAX') && DOING_AJAX ) || in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ) ) 
			return;

		// Load meta boxes
		add_action( 'admin_menu', array( &$this, 'add_meta_box' ) );

		// Handle campaign term options storage
		add_action( 'save_post', array( &$this, 'save_meta' ) );
		add_action( 'delete_post', array( &$this, 'delete_meta' ) );
	}

	/**
	 * Add adv main meta box
	 */
	function add_meta_box() {
		add_meta_box( 'rssi_taxonomy_filter', __( 'Attach posts to:', 'rssi' ), array( &$this, 'show_meta_box' ), rssi_get_campaign_post_type(), 'normal' );
	}

	/**
	 * Show meta box content
	 * @param  object $post Current post object
	 * @return void
	 */
	function show_meta_box( $post ) {
		require( RSSI_ABSPATH . '/modules/map-taxonomy/meta.tpl.php' );
	}

	/**
	 * Save current page meta
	 * @param  int $post_id Current post ID
	 * @return void
	 */
	function save_meta( $post_id ) {
		$post = get_post( $post_id );
		if ( rssi_get_campaign_post_type() !== $post->post_type ) {
			return;
		}

		if ( isset( $_POST['rssi'] ) && isset( $_POST['rssi']['terms'] ) ) {
			update_campaign_meta( $post_id, 'terms', $_POST['rssi']['terms'] );
		} else {
			delete_campaign_meta( $post_id, 'terms' );
		}
	}

	/**
	 * Delete campaign term meta on campaign removal
	 * @param  int $post_id Campaign id
	 * @return void
	 */
	function delete_meta( $post_id ) {
		delete_campaign_meta( $post_id, 'terms' );
	}

}
new RSSI_Module_Taxonomy_Map_Meta;