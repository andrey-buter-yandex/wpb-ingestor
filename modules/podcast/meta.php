<?php
/**
* Handle taxonomy map meta inside campaign
*
* @todo 
*/

class RSSI_Module_Podcast_Meta {

	function __construct() {
		global $pagenow;

		if ( is_admin() ) {
			add_action( 'init', array( &$this, 'init_meta' ) );
		}
	}

	public function init_meta() {
		global $pagenow;

		if ( !( defined('DOING_AJAX') && DOING_AJAX || in_array( $pagenow, array( 'post.php', 'post-new.php' )) ) )
			return;

		// Load meta boxes
		add_action( 'admin_menu', array( &$this, 'add_meta_box' ) );

		// Handle campaign term options storage
		add_action( 'save_post', array( &$this, 'save_meta' ) );
		add_action( 'delete_post', array( &$this, 'delete_meta' ) );
	
	}

	/**
	 * Add adv main meta box
	 */
	function add_meta_box() {
		add_meta_box( 'rssi_podcast', __( 'Podcast:', 'rssi' ), array( &$this, 'show_meta_box' ), rssi_get_campaign_post_type(), 'normal' );
	}

	/**
	 * Show meta box content
	 * @param  object $post Current post object
	 * @return void
	 */
	function show_meta_box( $post ) {
		require( RSSI_ABSPATH . '/modules/podcast/meta.tpl.php' );
	}

	/**
	 * Save current page meta
	 * @param  int $post_id Current post ID
	 * @return void
	 */
	function save_meta( $post_id ) {
		$post = get_post( $post_id );

		if ( rssi_get_campaign_post_type() !== $post->post_type || !isset($_POST) ) {
			return;
		}

		if ( isset( $_POST['rssi'] ) && isset( $_POST['rssi']['podcast'] ) ) {

			// We are inside post edit screen so we can provide podcast validation
			// if ( $this->validate_feed() ) {
				update_campaign_meta( $post_id, 'podcast', 1 );

				// Set campaign 'episode' as campaign post type 
				update_campaign_meta( $post_id, 'post_type', 'episode' );
			// }
		} else {
			delete_campaign_meta( $post_id, 'podcast' );

			if ( 'episode' == get_campaign_meta( $post_id, 'post_type', true ) ) {
				delete_campaign_meta( $post_id, 'post_type' );
			}
				
		}
	}

	/**
	 * Validate podcast feed
	 * @return bool|object Return true if feed is valid or wp error 
	 */
	private function validate() {
		
	}

	/**
	 * Delete campaign term meta on campaign removal
	 * @param  int $post_id Campaign id
	 * @return void
	 */
	function delete_meta( $post_id ) {
		delete_campaign_meta( $post_id, 'podcast' );
	}

}
new RSSI_Module_Podcast_Meta;