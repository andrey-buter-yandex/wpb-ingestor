<?php
/**
* Import posts as episodes and attach to podcast
*
* @todo  Remove actions that may have been set from previous campaign import on campaign done action
*/

class RSSI_Module_Podcast {

	function __construct() {

		// Include meta handler class
		require( RSSI_ABSPATH . '/modules/podcast/meta.php' );

		// Maybe start podcast import
		add_action( 'rssi_import_campaign', array( &$this, 'import_podcast' ) );
	}

	/**
	 * Maybe start podcast import
	 * @param  int $campign_id Current campaign id
	 * @return void
	 */
	public function import_podcast( $campign_id ) {
		if ( get_campaign_meta( $campign_id, 'podcast', true ) ) {

			// Set episode post_type for imported posts
			add_filter( 'rssi_import_post_default', array( &$this, 'set_post_type' ), 10, 2 );

			// Attach itunes meta to imported post
			add_action( 'rssi_import_post', array( &$this, 'set_meta' ), 10, 2 );
		}
	}

	/**
	 * Set episode post type for all import ids
	 * @param [type] $post_arr   [description]
	 * @param [type] $campign_id [description]
	 */
	public function set_post_type( $postarr, $campign_id ) {
		$postarr['post_type'] = 'episode';
		return $postarr;
	}

	/**
	 * Set episode specific meta
	 * @param int $post_id    Imported epidode id
	 * @param object $item    SimplePie item object
	 */
	public function set_meta( $post_id, $item ) {
		$post = get_post( $post_id );

		// Get episode audio
		if ( $enclosure = $item->get_enclosure( 0 ) ) {

			// Set Episode audio
			if ( isset( $enclosure->link ) ) {
				update_post_meta( $post_id, 'episode_audio', $enclosure->link );

				// Do enclosure
				$content = sprintf( '%s<a href="%s">%s</a>', $post->post_content, $enclosure->link, $post->post_title );
				do_enclose( $content, $post_id );
			}

			// Notify episode enclose
			do_action( 'enclose_episode', $post_id );
			// do_enclose( $content, $post_id );

			// Attach itunes meta if it presents
			if ( !empty( $enclosure->duration ) && is_int( $enclosure->duration ) ) {
				update_post_meta( $post_id, 'episode_audio_duration', $enclosure->duration );
			}
		}

	}


}

/**
 * Init module after plugin core loaded
 */
add_action( 'rssi_after_setup', 'init_rssi_module_podcast' );
function init_rssi_module_podcast() {
	new RSSI_Module_Podcast;
}