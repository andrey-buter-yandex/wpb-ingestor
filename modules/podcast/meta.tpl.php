<?php 
global $post;
?>
<div class="rss-podcast">
	<div class="rssi-row label-row">
		<label for="rssi_podcast_feed" class="main-label">This is podcast feed</label>
		<input type="checkbox" name="rssi[podcast]" id="rssi_podcast_feed" value="1" <?php checked( '1', get_campaign_meta( $post->ID, 'podcast', true ) ); ?> />
	</div>
</div>