<?php
/**
* Attach imported post to specified terms
*
* @todo 
*/

class RSSI_Module_Taxonomy_Map {

	function __construct() {

		// Include meta handler class
		require( RSSI_ABSPATH . '/modules/map-taxonomy/meta.php' );

		// Add checked campign terms to default postarr
		add_filter( 'rssi_import_post_default', array( &$this, 'attach_terms' ), 10, 2 );
	}

	/**
	 * Attach terms to postarr before post insert
	 * @param  array   $postarr     Postarr data to insert as post
	 * @param  integer $campaign_id Campaign id that is processed
	 * @return array                Postarr with tax_input if there were terms to attach
	 */
	public function attach_terms( $postarr = array(), $campaign_id = 0 ) {
		if ( $terms = get_campaign_meta( $campaign_id, 'terms', true ) ) {
			$postarr['tax_input'] = $terms;
		}

		return $postarr;
	}

}

/**
 * Init module after plugin core loaded
 */
add_action( 'rssi_after_setup', 'init_rssi_module_taxonomy_map' );
function init_rssi_module_taxonomy_map() {
	new RSSI_Module_Taxonomy_Map;
}