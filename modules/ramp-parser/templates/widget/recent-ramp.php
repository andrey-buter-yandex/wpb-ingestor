<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Itmwp2
 * @since Itmwp2 1.0
 *
 * @uses  $query_recent recent posts query
 */
?><article <?php post_class( 'compact bg-secondary' ); ?>>
	
	<?php if ( is_ramp_show_image() ): ?>
		<a href="<?php the_permalink() ?>" <?php ramp_link_atts() ?>><?php the_post_thumbnail( 'thumbnail-small' ); ?></a>
	<?php endif ?>
	
	<h5 class="title">
		<a class="inverse" href="<?php the_permalink(); ?>" <?php ramp_link_atts() ?>><?php the_title(); ?></a>
	</h5>

	<?php if ( is_ramp_show_content() ): ?>
		<div class="content"><?php the_content() ?></div>
	<?php endif ?>

	<span class="ramp-play-btn">
		<a href="<?php the_permalink() ?>" <?php ramp_link_atts() ?>>
			<?php if ( $src = ramp_get_play_btn_image_src() ): ?>
				<img src="<?php echo $src ?>" alt="<?php echo ramp_get_image_name( $src ) ?>" />
			<?php endif ?>
		</a>
	</span>
</article><?php 