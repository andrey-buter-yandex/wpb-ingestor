
<div class="Ramp_Containter">
	<div class="Ramp_Feed_Play">	
		<a href="<?php echo get_post_meta( get_the_ID(), 'post_xml_url', true ) ?>" <?php ramp_link_atts() ?>><?php the_post_thumbnail( 'thumbnail-small' ) ?></a>
	</div>
	<div class="Ramp_Feed_Text">
		<div class="Ramp_Rec_Feed_Label">Mentioned: </div>  
		<div class="Ramp_Feed_Keywords"><?php echo get_the_content() ?></div>
	</div>
</div>