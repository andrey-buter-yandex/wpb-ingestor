<?php 

/**
 * XML item 
 * similarly class SimplePie_Item
 * Use methods similarly SimplePie_Item methods
 */
class XML_Item
{	
	function __construct( $data )
	{
		// array to object
		if ( is_array( $data ) )
			$data = json_decode( json_encode( $data ), FALSE );

		$this->parse_args( $data );
	}

	function parse_args( $data )
	{
		foreach ( $data as $key => $arg )
			$this->{$key} = $arg;
	}

	public function get_id()
	{
		if ( isset( $this->post_id ) )
			return $this->post_id;

		if ( isset( $this->post_url ) )
			return $this->post_url;
	}

	public function get_title()
	{
		return isset( $this->post_title ) ? $this->post_title : '';
	}

	public function get_url()
	{
		return isset( $this->post_url ) ? $this->post_url : '';
	}

	public function get_image()
	{
		return isset( $this->post_image ) ? $this->post_image : '';
	}

	public function get_content()
	{
		if ( !isset( $this->post_content ) )
			return;

		return 'Mentioned: '. implode( ', ', $this->post_content );

		// if ( isset( $content ) AND !empty( $content ) ) {
		// 	$content = '
		// 		<div class="Ramp_Feed_Text">
		// 			<div class="Ramp_Rec_Feed_Label">Mentioned: </div>  
		// 			<div class="Ramp_Feed_Keywords">'. implode( ', ', $content ) .'</div>
		// 		</div>';
		// }

		// $output = '
		// 	<div class="Ramp_Containter">
		// 		<div class="Ramp_Feed_Play">	
		// 			<a href="'. $this->post_url .'" target="_blank"><img src="'. $this->post_image .'"></a>
		// 		</div>
		// 		'. $content .'
		// 	</div>';

		// return $output;
	}

	public function get_date( $format )
	{
		$post_date = isset( $this->post_date ) ? $this->post_date : '';

		if ( empty( $post_date ) )
			return $post_date;

		if ( false === strtotime( $post_date ) )
			return $post_date;

		return date( $format, strtotime( $post_date ) );
	}
}