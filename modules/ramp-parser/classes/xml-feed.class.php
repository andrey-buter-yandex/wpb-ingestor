<?php 

/**
 * XML item 
 * similarly class SimplePie_Item
 * Use methods similarly SimplePie_Item methods
 */
class XML_Feed extends RSSI_Feed
{	
	public $url;

	/**
	 * Get SimplePie 
	 * @return Object Will return either SimplePie object or WP_Error object
	 */
	function get_feed() 
	{
		if ( isset( $this->feed ) )
			return $this->feed;

		if ( isset( $this->errors ) )
			return $this->errors;

		$xml = @file_get_contents( $this->url );

		if ( false === $xml )
			return $this->add_error( 'xml-error', 'No such host is known.' );

		$xml_obj = new SimpleXMLElement( $xml );

		if ( !isset( $xml_obj->ResultSet->Results->CompleteResult ) )
			return $this->add_error( 'xml-error', 'No such xml data is known.' );

		$this->feed = $xml_obj->ResultSet->Results->CompleteResult;

		return $this->feed;
	}

	/**
	 * Get feed items
	 * @param  int $page  Feed page to start with
	 * @param  int $limit Maximum number of items to return
	 * @return (array|object)  Array of 
	 */
	public function get_items( $page = 0, $limit = 0 ) 
	{
		$feed = $this->get_feed();

		if ( is_wp_error( $feed ) )
			return $feed;

		$maxitems = $this->get_item_quantity( $limit );

		if ( empty( $maxitems ) )
			return new WP_Error( 'empty-feed', 'No items in the feed' );

		// Calculate offset for specified page
		$offset = $page * $limit;

		if ( $offset > $maxitems )
			return new WP_Error( 'no-more-items', 'No more items in the feed' );

		// Build an array of all the items, starting with element 0 (first element).
    	return $this->get_ramp_items( $offset, $maxitems );
	}

	function get_ramp_items( $start, $end )
	{
		$items = array();

		foreach ( $this->feed as $key => $item ) {
			$post_url     = (array) $item->EpisodeMetaData['landing-url'];
			$post_title   = (array) $item->EpisodeMetaData->Title;
			$post_content = (array) $item->EpisodeMetaData->KeyWords;
			$post_image   = (array) $item->SeriesMetaData['image-url'];
			// $post_id      = (array) $item->SeriesMetaData['id']; // no post_id
			// $post_id      = (array) $item->attributes()->id; // maybe post_id
			$post_id      = (array) $item->EpisodeMetaData['id'];
			$post_date    = (array) $item->EpisodeMetaData['pub-date'];

			$data = array(
				'post_url'     => $post_url[0],
				'post_title'   => $post_title[0],
				'post_content' => $post_content['KeyWord'],
				'post_image'   => $post_image[0],
				'post_id'      => $post_id[0],
				'post_date'    => $post_date[0],
			);

			$items[] = new XML_Item( $data );
		}

		// Slice the data as desired
		if ( $end === 0 )
			return array_slice( $items, $start );
		else
			return array_slice( $items, $start, $end );

		return $items;
	}

	/**
	 * Get the number of items in the feed
	 *
	 * This is well-suited for {@link http://php.net/for for()} loops with
	 * {@see SimplePie->get_item()}
	 *
	 * @param int $max Maximum value to return. 0 for no limit
	 * @return int Number of items in the feed
	 */
	public function get_item_quantity( $max = 0 )
	{
		$max = (int) $max;
		$qty = count( $this->feed );
		if ( $max === 0 )
			return $qty;
		else
			return ( $qty > $max ) ? $max : $qty;
	}
}