/**
 * Main mathys theme js file
 */

"use strict";

(function($){
	jQuery(window).load(function(){ 
		// Initialize slider
		if ( typeof $().carouFredSel == 'function' ) {
			$('.js-ramp-slider').map(function(){
				var wrapper = $(this),
					slider  = wrapper.find('.posts-container');

				var maxHeight = 16;

				slider.children().css('max-width', wrapper.width());

				slider.children().each(function() {
					if ( $(this).height() > maxHeight )
						maxHeight = $(this).height();
				});

				if ( 16 == maxHeight )
					wrapper.css('padding-top', '6px');

				slider.carouFredSel({
					height		: maxHeight,
					responsive  : true,
					swipe       : true,
					items		: {
						visible		: 1,
						width       : 290
					},
					auto		: false,
					prev: wrapper.find('.js-prev'),
					next: wrapper.find('.js-next'),
					onCreate: function(data) {
						data.items.prevObject.css('display','inline-block');
					}
				});
			});
		}

	}); // End of jQuery shell function
})(jQuery);