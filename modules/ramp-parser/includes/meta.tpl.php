<?php 
global $post;
?>
<div class="rss-xml_parser rss-import">
	<div class="rssi-row label-row">
		<label for="rssi_xml_parser" class="main-label">Enable RAMP</label>
		<input type="checkbox" name="rssi[xml_parser][xml_parser]" id="rssi_xml_parser" value="1" <?php checked( '1', get_campaign_meta( $post->ID, 'xml_parser', true ) ); ?> />
	</div>

	<?php $post_view = get_campaign_meta( $post->ID, 'post_view', true ); ?>
	<div class="rssi-row label-row">
		<label for="rssi_xml_parser[post_view]" class="main-label">Special Options view</label>
		<select name="rssi[xml_parser][post_view]">
			<option value="1" <?php selected( '1', $post_view ) ?>>Ramp View 1</option>
			<option value="2" <?php selected( '2', $post_view ) ?>>Ramp View 2</option>
			<option value="3" <?php selected( '3', $post_view ) ?>>Ramp View 3</option>
		</select>
	</div>
</div>