<?php
/**
 * Register advertise post type
 * each post is single advertisement
 */

add_action( 'init', 'create_ramp_post_type' );

function create_ramp_post_type() {
	register_post_type( 
		'ramp',
		array(
			'labels' => array(
				'name' => __( 'RAMP', 'adv' ),
				'singular_name' => __( 'RAMP post', 'adv' ),
				'all_items' => __( 'All RAMP posts', 'adv' ),
				'add_new' => __( 'Add RAMP post', 'adv' ),
				'add_new_item' => __( 'Add New RAMP post', 'adv' ),
				'edit' => __( 'Edit', 'adv' ),
				'edit_item' => __( 'Edit RAMP post', 'adv' ),
				'new_item' => __( 'New RAMP post', 'adv' ),
				'view' => __( 'View RAMP post', 'adv' ),
				'view_item' => __( 'View RAMP post', 'adv' ),
				'search_items' => __( 'Search RAMP post', 'adv' ),
				'not_found' => __( 'No RAMP posts found', 'adv' ),
				'not_found_in_trash' => __( 'No RAMP posts found in Trash', 'adv' ),
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => false,

			'menu_position' => 32,
			'hierarchical' => false,
			'query_var' => true,
			'supports' => array( 'title', 'editor', 'custom-fields' /*, 'thumbnail' */ ),
			'rewrite' => false,
			'can_export' => true,
		)
	);
};