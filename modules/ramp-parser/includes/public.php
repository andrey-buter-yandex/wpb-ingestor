<?php 

/**
 * Get image name by src
 */
function ramp_get_image_name( $src = '' ) {
	if ( !$src )
		return;

	$name = wp_basename( $src );
	$name = explode( '.', $name );

	$name = array_slice( $name, 0, count( $name ) - 1 );

	return implode( '.', $name );
}


function ramp_link_atts() {
	$target = get_post_meta( get_the_ID(), 'post_target', true );

	if ( 'blank' != $target )
		return;

	echo ' target="_blank" ';
}

/**
 * Return widget global args
 */
function ramp_get_widget_global_args() {
	if ( !isset( $GLOBALS['xml_parser_args'] ) )
		return array();

	return $GLOBALS['xml_parser_args'];
}

/**
 * Return widget global arg by key
 */
function ramp_get_widget_global_arg( $key = '' ) {
	if ( !$key )
		return;

	$args = ramp_get_widget_global_args();

	if ( !isset( $args[$key] ) )
		return;

	return $args[$key];
}

/**
 * Used in post template
 */
function ramp_get_play_btn_image_src() {
	$play_btn = ramp_get_widget_global_arg( 'play_btn' );

	if ( !$play_btn )
		return;

	return $play_btn;
}

/**
 * Check show ramp-post post_content or no
 */
function is_ramp_show_content() {
	$show_content = ramp_get_widget_global_arg( 'show_content' );

	if ( $show_content )
		return true;

	return false;
}

/**
 * Check show ramp-post post image or no
 */
function is_ramp_show_image() {
	$show_image = ramp_get_widget_global_arg( 'show_image' );

	if ( $show_image )
		return true;

	return false;
}

/**
 * Filter RAMP-post permalink
 */
add_filter( 'the_permalink', 'ramp_the_permalink' );
function ramp_the_permalink( $link ) {
	if ( 'ramp' == get_post_type() && $url = get_post_meta( get_the_ID(), 'post_url', true ) )
		$link = $url;

	return $link;
}