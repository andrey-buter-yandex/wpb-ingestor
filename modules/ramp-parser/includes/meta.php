<?php
/**
* Handle taxonomy map meta inside campaign
*
* @todo 
*/

class RSSI_Module_XML_Parser_Meta {

	// public $meta_key = 'xml_parser';
	public $tpl_path = 'ramp-parser';
	public $meta_box_id = 'rssi_xml_parser';
	public $meta_box_title = 'RAMP:';
	public $post_type = '';

	function __construct() {
		if ( is_admin() ) {
			$this->post_type = rssi_get_campaign_post_type();
			$this->meta_keys = array(
				'xml_parser',
				'post_view'
			);
			add_action( 'init', array( &$this, 'init_meta_box' ) );
		}
	}

	public function init_meta_box() {
		global $pagenow;

		if ( !(  ( defined('DOING_AJAX') && DOING_AJAX ) || in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ) ) 
			return;

		// Load meta boxes
		add_action( 'admin_menu', array( &$this, 'add_meta_box' ) );

		// Handle campaign term options storage
		add_action( 'save_post',   array( &$this, 'save_meta'   ) );
		add_action( 'delete_post', array( &$this, 'delete_meta' ) );
	}

	/**
	 * Add adv main meta box
	 */
	function add_meta_box() {
		add_meta_box( $this->meta_box_id, __( $this->meta_box_title, 'rssi' ), array( &$this, 'show_meta_box' ), $this->post_type, 'normal' );
	}

	/**
	 * Show meta box content
	 * @param  object $post Current post object
	 * @return void
	 */
	function show_meta_box( $post ) {
		require( RSSI_ABSPATH . '/modules/'. $this->tpl_path .'/includes/meta.tpl.php' );
	}

	/**
	 * Save current page meta
	 * @param  int $post_id Current post ID
	 * @return void
	 */
	function save_meta( $post_id ) {
		if ( !$_POST )
			return;

		$post = get_post( $post_id );

		if ( $this->post_type !== $post->post_type )
			return;

		if ( !isset( $_POST['rssi']['xml_parser'] ) )
			return;

		$data = array_map( 'stripslashes_deep', $_POST['rssi']['xml_parser'] );

		foreach ( $this->meta_keys as $key ) {
			$value = $data[$key];

			if ( !empty( $value ) ) {
				update_campaign_meta( $post_id, $key, $value );
			} else {
				delete_campaign_meta( $post_id, $key );
			}
		}
	}

	/**
	 * Delete campaign term meta on campaign removal
	 * @param  int $post_id Campaign id
	 * @return void
	 */
	function delete_meta( $post_id ) {
		foreach ( $this->meta_keys as $key ) {
			delete_campaign_meta( $post_id, $key );
		}
	}

}
new RSSI_Module_XML_Parser_Meta;