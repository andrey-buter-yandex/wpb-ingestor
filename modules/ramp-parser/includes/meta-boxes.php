<?php
/**
* Handle taxonomy map meta inside campaign
*
* @todo 
*/


add_action( 'init', 'add_ramp_meta_boxes' );
function add_ramp_meta_boxes() {
	global $meta_boxes;
	$prefix = 'post_';
	$meta_boxes[] = array(
		'id'       => 'ramp_meta',
		'title'    => __( 'Attributes', 'wayne_forte' ),
		'pages'    => array( 'ramp' ),
		'context'  => 'normal',
		'priority' => 'high',
		'fields'   => array(
			array(
				'label' => 'External URL Links to',
				'id' => $prefix . 'url',
				'type' => 'text'
			),
			array(
				'label' => 'Featured Image',
				'id' => $prefix . 'image',
				'type' => 'upload'
			),
			array(
				'label' => 'Target',
				'id' => $prefix . 'target',
				'type' => 'select',
				'choices'     => array(
					array(
						'label'       => 'Same window',
						'value'       => 'same'
					),
					array(
						'label'       => 'New window',
						'value'       => 'blank'
					)
				),
			),
		)
	);
}

