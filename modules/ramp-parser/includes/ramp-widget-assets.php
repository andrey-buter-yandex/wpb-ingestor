<?php 

/**
 * Enable Ramp Widge Assets
 */
class RAMP_Widget_Asssets
{
	private static $widget_base_id = 'ramp_widget';
	private static $is_enable_scripts = false;

	public static function load()
	{
		add_action( 'wp_enqueue_scripts', array( __CLASS__, 'enqueue_scripts' ) );
	}

	public function enqueue_scripts()
	{
		if ( !self::is_active_widget() )
			return;

		if ( !self::is_enable_scripts() )
			return;

		$src = plugin_dir_url( Ramp_Parser::$abs_path . '/assets/some-file.php' );

		wp_enqueue_script( 'caroufredsel-swipe-js', $src .'js/jquery.touchSwipe.min.js', array( 'jquery' ), '1', true );
		wp_enqueue_script( 'caroufredsel-js', $src .'js/jquery.carouFredSel-6.2.1-packed.js', array( 'caroufredsel-swipe-js' ), '1', true );
		wp_enqueue_script( 'ramp-common-js',  $src .'js/ramp-common.js', array( 'jquery' ), '1', true );

		wp_enqueue_style( 'ramp-styles', $src .'css/ramp-styles.css' );
	}

	/**
	 * find enabled caroufredsel script
	 */
	// function find_enabled_script()
	// {
	// 	global $wp_scripts;

	// 	if ( !$wp_scripts->registered )
	// 		return;

	// 	foreach ( $wp_scripts->registered as $script_data ) {
	// 		if ( false === strpos( $script_data->src, 'carouFredSel' )
	// 			continue;

	// 		return true;
	// 	}

	// 	var_dump($wp_scripts->registered);
	// }

	/**
	 * Check is enable scripts in some active widget
	 */
	private static function is_enable_scripts_in_widget( $widget_data )
	{
		$option_name = $widget_data['callback'][0]->option_name;

		$options = get_option( $option_name );

		if ( !$options )
			return false;

		foreach ( $options as $widget_options ) {
			if ( true === $widget_options['enable_scripts'] )
				return true;
		}

		return false;
	}

	/**
	 * Check is enable scripts
	 */
	private static function is_enable_scripts()
	{
		global $wp_registered_widgets;

		foreach ( $wp_registered_widgets as $widget_id => $widget_data ) {
			$id_base = _get_widget_id_base( $widget_id );

			if ( self::$widget_base_id != $id_base )
				continue;

			return self::is_enable_scripts_in_widget( $widget_data );
		}

		return false;
	}

	/**
	 * Check is active widget in some sidebar
	 */
	private static function is_active_widget()
	{
		return is_active_widget( false, false, self::$widget_base_id, true );
	}
}

RAMP_Widget_Asssets::load();