<?php 

add_action( 'widgets_init', 'xml_parser_widgets_init' );
function xml_parser_widgets_init() {
	register_widget( 'RAMP_Widget' );
}


/**
 * Subscribe widget class
 *
 * @since 2.8.0
 */
class RAMP_Widget extends WP_Widget 
{
	function __construct() 
	{
		$widget_ops = array( 'description' => __( "Recent RAMP posts" ) );
		parent::__construct( 'ramp_widget', __('RAMP widget'), $widget_ops );
	}

	function widget( $args, $instance ) 
	{
		extract( $args );

		$number = $instance['number'] ? absint( $instance['number'] ) : 10;
		$title  = $instance['title']  ? $instance['title']  : '';
		$image  = $instance['image']  ? $instance['image']  : '';
		$view   = $instance['view']   ? $instance['view']   : '1';
		$filter = $instance['filter'] ? $instance['filter'] : false;

		if ( $title )
			$title = $before_title . $title . $after_title;

		$before_widget = str_replace( 'widget_ramp_widget', 'widget_ramp_widget widget_ramp_widget-view-'. $view, $before_widget );

		$wrapper = $wrapper_end = '';

		$show_image = true;

		if ( 3 == $view ) {
			$wrapper     = '<div class="js-ramp-slider ramp-slider-wrapper">';
			$prev_next   = '
				<div class="prev-next-navigation">
					<a class="js-prev prev nav-item" href="#">
						<span class="inn">
							<span class="treangle"></span>
						</span>
					</a>
					<a class="js-next next nav-item" href="#">
						<span class="inn">
							<span class="treangle"></span>
						</span>
					</a>
				</div>';
			$wrapper_end = $prev_next .'</div>';
			$show_image  = false;
		}

		$r = new WP_Query( array( 
			'post_type'      => 'ramp',
			'posts_per_page' => $number, 
			'no_found_rows'  => true, 
			'post_status'    => 'publish', 
			'ignore_sticky_posts' => true 
		) );

		$global_args = array(
			'play_btn'     => $image,
			'show_content' => $filter,
			'show_image'   => $show_image
		);

		if ( $r->have_posts() ) {

			echo $before_widget;

			echo $title;

			echo $wrapper;

				echo '<div class="posts-container posts-container-view-'. $view .'">';

					$GLOBALS['xml_parser_args'] = $global_args;
					// $GLOBALS['xml_parser_play'] = $image;
					// $GLOBALS['xml_parser_show_content'] = $filter;

					while ( $r->have_posts() ) { 
						$r->the_post();
						$this->get_template( 'widget', 'recent-ramp' );
					}

					unset( $GLOBALS['xml_parser_args'] );
					// unset( $GLOBALS['xml_parser_play'] );
					// unset( $GLOBALS['xml_parser_show_content'] );

				echo '</div>';

			echo $wrapper_end;

			echo $after_widget; 
		}

		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();
	}

	function get_template( $slug, $template_name ) {
		if ( file_exists( TEMPLATEPATH .'/'. $slug .'/'. $template_name ) ) {
			if ( function_exists( 'get_theme_part' ) )
				get_theme_part( $slug, $template_name );
			else
				get_template_part( $slug .'/'. $template_name );
		} else {
			Ramp_Parser::locate_template( 'widget/'. $template_name );
		}
	}

	function update( $new_instance, $old_instance ) 
	{
		$instance = $old_instance;
		$instance['title']  = strip_tags($new_instance['title']);
		$instance['image']  = strip_tags($new_instance['image']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['view']   = (int) $new_instance['view'];
		$instance['filter'] = isset($new_instance['filter']);

		// enable_scripts only for view == 3
		$instance['enable_scripts'] = ( 3 == $instance['view'] ) ? isset( $new_instance['enable_scripts'] ) : false;

		return $instance;
	}

	function form( $instance ) 
	{
		$title  = isset( $instance['title'] )  ? esc_attr( $instance['title'] ) : '';
		$image  = isset( $instance['image'] )  ? esc_attr( $instance['image'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] )  : 10;
		$view   = isset( $instance['view'] )   ? esc_attr( $instance['view'] )  : '1';

		$enable_scripts = isset( $instance['enable_scripts'] ) ? $instance['enable_scripts'] : 0
?>
		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'image' ); ?>"><?php _e('Play button image url:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('image'); ?>" name="<?php echo $this->get_field_name('image'); ?>" type="text" value="<?php echo $image; ?>" />
		</p>

		<p>
			<input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Show post content'); ?></label>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id('view'); ?>"><?php _e( 'Select post view:' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id('view'); ?>" name="<?php echo $this->get_field_name('view'); ?>">
				<option value="1" <?php selected( $view, '1' ); ?>><?php _e('View 1'); ?></option>
				<option value="2" <?php selected( $view, '2' ); ?>><?php _e('View 2'); ?></option>
				<option value="3" <?php selected( $view, '3' ); ?>><?php _e('View 3'); ?></option>
			</select>
			</select>
		</p>

		<p>
			<input id="<?php echo $this->get_field_id('enable_scripts'); ?>" name="<?php echo $this->get_field_name('enable_scripts'); ?>" type="checkbox" <?php checked( $enable_scripts ); ?> />
			<label for="<?php echo $this->get_field_id('enable_scripts'); ?>"><?php _e( 'Enable Slider for View 3' ); ?></label>
		</p>
<?php
	}
}