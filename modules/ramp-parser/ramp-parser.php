<?php 

/**
 * RAMP XML Parser
 */
class Ramp_Parser 
{
	static $abs_path = '/modules/ramp-parser';

	function __construct()
	{
		self::$abs_path = RSSI_ABSPATH . self::$abs_path;

		if ( is_admin() OR defined( 'DOING_CRON' ) ) {
			// load xml classes
			$this->load_file( 'classes', 'xml-item.class' );
			$this->load_file( 'classes', 'xml-feed.class' );

			add_filter( 'rssi_feed_class', array( $this, 'check_feed_class' ), 10, 2 );

			// Save post meta after insert post
			add_action( 'rssi_import_post', array( &$this, 'save_post_meta' ), 10, 3 );
			add_filter( 'rssi_import_post_default', array( $this, 'rssi_import_post_default' ), 10, 2 );
		}

		$this->load_file( 'includes', 'model' );

		if ( is_admin() ) {	
			$this->load_file( 'includes', 'meta'  );			
			$this->load_file( 'includes', 'meta-boxes' );
		} else {
			// add_filter( 'the_content', array( $this, 'content_template' ) );
			add_filter( 'post_thumbnail_html', array( $this, 'replace_post_thumbnail_html' ), 10, 5 );
		}

		$this->load_file( 'includes', 'public' );
		$this->load_file( 'includes', 'widgets' );
		$this->load_file( 'includes', 'ramp-widget-assets' );

		add_filter( 'cron_schedules', array( $this, 'add_new_schedules_recurrence' ) );
	}

	function rssi_import_post_default( $postarr, $campaign_id ) 
	{
		if ( !$this->is_xml_parser( $campaign_id ) )
			return $postarr;

		$postarr['post_type'] = 'ramp';

		return $postarr;
	}


	/**
	 * Redefine parser class to XML_Feed()
	 */
	function check_feed_class( $feed, $campaign_id )
	{
		if ( !$this->is_xml_parser( $campaign_id ) )
			return $feed;

		$feed = new XML_Feed();

		return $feed;
	}

	function is_xml_parser( $campaign_id ) 
	{
		if ( isset( $this->is_xml_parser ) )
			return $this->is_xml_parser;

		$this->is_xml_parser = get_campaign_meta( $campaign_id, 'xml_parser', true ) ? true : false;

		return $this->is_xml_parser;
	}

	/**
	 * Other functions
	 **************************************/

	/**
	 * Add new schedules recurrence
	 */
	public function add_new_schedules_recurrence( $schedules = array() )
	{
		// $schedules['every_1_min']  = array( 'interval' => 1  * MINUTE_IN_SECONDS, 'display' => __( 'Every 1 minute'   ) );
		$schedules['every_10_min'] = array( 'interval' => 10 * MINUTE_IN_SECONDS, 'display' => __( 'Every 10 minutes' ) );
		return $schedules;
	}

	/**
	 * Save post meta after import new post
	 */
	function save_post_meta( $post_id, $item, $campaign_id ) 
	{
		if ( !$this->is_xml_parser( $campaign_id ) )
			return;

		if ( $item->get_url() )
			update_post_meta( $post_id, 'post_url', $item->get_url() );

		if ( $item->get_image() )
			update_post_meta( $post_id, 'post_image', $item->get_image() );
	}

	/**
	 * Add to post thumbnail html parse image url
	 */
	function replace_post_thumbnail_html( $html, $post_id, $post_thumbnail_id, $size, $attr )
	{
		global $_wp_additional_image_sizes;

		if ( !empty( $html ) OR has_post_thumbnail( $post_id ) )
			return $html;

		if ( !$image = get_post_meta( $post_id, 'post_image', true ) )
			return $html;

		$sizes = $_wp_additional_image_sizes[$size];

		$atts = '';

		if ( isset( $sizes['width'] ) ) 
			$atts = 'width="'. $sizes['width'] .'"';
		elseif ( isset( $sizes['height'] ) ) 
			$atts = 'height="'. $sizes['height'] .'"';

		if ( 'slide' == $size )
			$atts = '';

		$alt = ramp_get_image_name( $image );

		$html = '<img class="attachment-thumbnail-small wp-post-image" src="'. $image .'" alt="'. $alt .'" '. $atts .' />';

		return $html;
	}

	/**
	 * Add to post thumbnail html parse image url
	 */
	function content_template( $content ) {
		if ( 'ramp' != get_post_type() )
			return $content;

		if ( empty( $content ) )
			return $content;

		// remove_filter( 'the_content', array( $this, 'content_template' ) );

		ob_start();
		self::locate_template( 'content' );
		$content = ob_get_clean();

		// add_filter( 'the_content', array( $this, 'content_template' ) );

		return $content;
	}

	/**
	 * Retrieve the name of the highest priority template file that exists.
	 */
	static function locate_template( $template_name ) {
		$located = '';

		if ( !$template_name )
			return $located;

		$template_name = $template_name .'.php';

		$path = self::$abs_path .'/templates/';

		if ( file_exists( $path . $template_name ) )
			$located = $path . $template_name;

		if ( '' != $located )
			load_template( $located, false );

		return $located;
	}

	function load_file( $folder, $file ) {
		require( self::$abs_path .'/'. $folder .'/'. $file .'.php' );
	}
}

/**
 * Init module after plugin core loaded
 */
// add_action( 'rssi_after_setup', 'load_xml_parser' );
add_action( 'init', 'load_xml_parser', 1 );
function load_xml_parser() {
	if ( !defined( 'RSSI_ENABLE_RAMP' ) )
		return;

	new Ramp_Parser;
}