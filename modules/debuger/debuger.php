<?php 

function wpb_debug( $args ) {
	$logs = get_option( 'wpb_ingestor_debug', array() );

	$logs[] = $args;

	update_option('wpb_ingestor_debug', $logs);
}


add_action( 'admin_notices', 'wpb_admin_notice' );
function wpb_admin_notice() {
	if ( isset( $_GET['debug'] ) ) {
		delete_option( 'wpb_ingestor_debug' );
	}

	$logs = get_option( 'wpb_ingestor_debug', array() );

	var_dump($logs);
}