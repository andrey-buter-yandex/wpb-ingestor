<?php
/**
* Prevent duplicates posts import
*
* 2 scenarios to prevent
* - First item in the feed is the last imported campaign post
* - Current processing post was already imported
*
* @todo 
*/

class RSSI_Module_Campaign_Unique_Posts {

	/**
	 * Hold current processing campaign id
	 * @var int
	 */
	private $campaign_id;

	function __construct() {
		// Exclude all items that were already imported
		add_filter( 'rssi_campaign_import_items', array( &$this, 'exclude_duplicates' ) );

		// Initialize filter rssi_import_campaign action
		//add_action( 'rssi_import_campaign', 'start_filtering' );

		// Store current campaign id
		// add_action( 'rssi_import_campaign', array( &$this, 'store_campaign_id' ) );

		// Save last imported id
		add_action( 'rssi_import_post', array( &$this, 'save_imported_item_id' ), 10, 2 );
	}


	/**
	 * Exclude all duplicates from $items array
	 * @param  [type] $items [description]
	 * @param  [type] $feed  [description]
	 * @return [type]        [description]
	 */
	public function exclude_duplicates( $items ) {
		global $wpdb;

		if ( is_array( $items ) ) {
			$hashes = '';

			// Collect all hashes
			foreach ( $items as $item ) {
				$hash = $this->get_unique_id( $item );
				$hashes .= empty( $hashes ) ? "'$hash'" : ",'$hash'";
			}

			// Get posts_ids that have this hashes as meta_value
			$hashes = $wpdb->get_col( "SELECT meta_value FROM $wpdb->postmeta WHERE meta_key = 'rssi_hash' AND meta_value IN ($hashes)" );
			

			// Unset all items with this hases 
			if ( !empty( $hashes ) ) {
				foreach ( $items as $key => $item ) {
					if ( in_array( $this->get_unique_id( $item ), $hashes ) ) {
						unset( $items[ $key ] );
					}
						
				}
			}			

			if ( empty( $items ) ) {
				$items = new WP_Error( 'no-new-posts', 'There were no new posts in the feed' );
			}
		}

		return $items;
	}

	public function save_imported_item_id( $post_id, $item ) {
		update_post_meta( $post_id, 'rssi_hash', $this->get_unique_id( $item ) );
	}

	/**
	 * Create uniqie item id if it's not define from xml
	 */
	public function get_unique_id( $item )
	{
		if ( $item->get_id() )
			return $item->get_id();

		$title = sanitize_title_with_dashes( $item->get_title() );
		$time = $item->get_date( 'U' );

		return $title .'-'. $time;
	}


	/**
	 * Initialize all filter actions here
	 * @param  int $campaign_id Current campaign id
	 * @return void              
	 */
	public function start_filtering( $campaign_id ) {

		// Store Current campaign id
		$this->campaign_id = $campaign_id;

		// Check if feed had new items to import
		add_filter( 'rssi_campaign_import_items', array( &$this, 'abort_import' ), 10, 2 );

		// Unique check before importing each item
		add_action( 'rssi_pre_import_post', array( &$this, 'exclude_duplicate' ) );

		// Store last imported post id
		add_action( 'rssi_import_post', array( &$this, 'store_imported_post_hash' ) );

		// Save last imported id when campaign import is finished
		add_action( 'rssi_campaign_imported', array( &$this, 'save_last_imported_post_hash' ) );

	}

	/**
	 * Abort campaign import if first item to import was already imported
	 * @param  array $items  SimplePie items array
	 * @param  object $feed  SimplePie object
	 * @return array|object  SimplePie items array if there are new items or WP_Error object if not
	 */
	public function filter_duplicates( $items, $feed ) {
		if ( !is_array( $items ) ) {

			$count = 0;
			foreach ( $items as $key => $item ) {

				// If first item in the array was already imported, abort import process with error message
				if ( 0 == $count && $this->is_last_imported_item( $item ) ) {
					return new WP_Error( 'no-new-items', __( 'There are no new items to import from this feed' ) );
				}

				// Collect items ids to check for duplicates in our db 

				$count++;
			}

			// Maybe Abort feed itself

			// Retrieve last campaign hash meta
			$last_hash = get_campaign_meta( $this->campaign_id, 'last_hash', true );

			if ( !empty( $last_hash ) ) {

				// Retrieve first item hash
				$item = array_shift( array_values( $items ) );
				$hash =$this->get_unique_id( $item );

				if ( $last_hash == $hash ) {
					return new WP_Error( 'no-new-items', __( 'There are no new items to import from this feed' ) );
				}
			}
		}
		return $items;
	}

	/**
	 * Check if passed item is the last previously imported item
	 * @param  [type]  $item [description]
	 * @return boolean       [description]
	 */
	private function is_last_imported_item( $item ) {
		
	}

	
	/**
	 * Store last imported post hash as cache
	 * @param  int $post_id  Imported post id
	 * @param  object $item  SimplePie item object
	 * @return void
	 */
	public function store_imported_post_hash( $post_id, $item ) {
		wp_cache_set( 'last_hash', $this->get_unique_id( $item ), 'rssi-unique' );
	}

	/**
	 * Update last item hash for campaign
	 * @param  int $campaign_id Processed campaign id
	 * @return void
	 */
	public function save_last_imported_post_hash( $campaign_id ) {
		if ( $hash = wp_cache_get( 'last_hash', 'rssi-unique' ) ) {
			update_campaign_meta( $campaign_id, 'last_hash', $hash );

			// Clear cache
			wp_cache_delete( 'import_hash', 'rssi-unique' );
		}
	}

	/**
	 * Store campaign id for internal use in other actions
	 * @param  int $campign_id Current campaign id
	 * @return void            
	 */
	public function store_campaign_id( $campaign_id ) {
		$this->campaign_id = $campaign_id;
	}

}

/**
 * Init module on campaign import start
 */
add_action( 'rssi_after_setup', 'init_rssi_module_campaign_unique_posts' );
function init_rssi_module_campaign_unique_posts() {
	new RSSI_Module_Campaign_Unique_Posts;
}