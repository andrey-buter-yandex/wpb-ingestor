<?php
/**
* Attach expiration post meta for all imported posts
*
*/

class RSSI_Module_Expiry_Map {

	function __construct() {

		add_action( 'rssi_import_campaign', array( &$this, 'map_expiry' ) );

		// We don't need to expire campaign itself
		add_action( 'save_post', array( &$this, 'unschedule_campaign_itself' ), 11 );

		// Include meta handler class
		// require( RSSI_ABSPATH . '/modules/map-taxonomy/meta.php' );
	}

	/**
	 * Emulate expiration form submit for post-expirator plugin
	 * @param  int $campaign_id Current campaign id
	 * @return void              
	 */
	public function map_expiry( $campaign_id ) {

		// Retrieve expiration post data
		if ( $ts = get_post_meta( $campaign_id, '_expiration-date', true ) && $options = get_post_meta( $campaign_id, '_expiration-date-options', true ) ) {

			// Mysitcal bug...
			$ts = get_post_meta( $campaign_id, '_expiration-date', true );

			// Make Campaign meta avaialable for all imported posts
			wp_cache_set( 'ts', $ts, 'campaign-expiry' );
			wp_cache_set( 'opitons', $options, 'campaign-expiry' );

			add_action( 'rssi_import_post', array( &$this, 'set_post_expiry' ) );

		} else {
			wp_cache_delete( 'ts', 'campaign-expiry' );
			wp_cache_delete( 'opitons', 'campaign-expiry' );

			remove_action( 'rssi_import_post', array( &$this, 'set_post_expiry' ) );
		}

	}

	/**
	 * Set post expiration using post-expiration functions
	 * @param int $post_id Imported post id
	 */
	public function set_post_expiry( $post_id ) {
		if ( function_exists( '_scheduleExpiratorEvent' ) ) {
			try {
				_scheduleExpiratorEvent( $post_id, wp_cache_get( 'ts', 'campaign-expiry' ), wp_cache_get( 'options', 'campaign-expiry' ) );
			} catch (Exception $e) {

				// Store exception data for debug mode 
			}
		}
	}

	public function unschedule_campaign_itself( $post_id ) {
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE )
			return;

		$post = get_post( $post_id );

		if ( $post->post_type != rssi_get_campaign_post_type() )
			return;

		if ( wp_next_scheduled( 'postExpiratorExpire', array( $post_id ) ) !== false )
			wp_clear_scheduled_hook( 'postExpiratorExpire', array( $post_id ) );

	}

}

/**
 * Init module after plugin core loaded
 */
add_action( 'rssi_after_setup', 'init_rssi_module_expiry_map' );
function init_rssi_module_expiry_map() {
	new RSSI_Module_Expiry_Map;
}