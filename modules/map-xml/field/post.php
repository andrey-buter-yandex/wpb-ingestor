<?php
/**
*
* Handle post attribute remap
* - render meta box
* - store xml mappging data as campaign meta
* - handle ajax request for available xml nodes
*
* This class shold not be used directly it should be abstracted by concrete post attribute class
*
*/

class RSSI_Map_Xml_Field_Post extends RSSI_Map_Xml_Field {

	/**
	 * Hold post attribute key
	 * @var string
	 */
	private $attribute;

	function __construct() {

		if ( is_admin() ) {

			// Action to render field form select
			add_action( 'rssi_remap_fields', array( &$this, 'get_field' ) );

			// Save post action
			add_action( 'rssi_remap_save', array( &$this, 'save' ) );
		}

		// Remap attribute action
		add_action( 'rssi_import_post_data', array( &$this, 'remap' ) );
	}

	/**
	 * Process post attriburte remap
	 * @param array $postarr Array of attributes for the post in import queue
	 * @param object $item   SimplePie item object
	 * @return array         Filtered array
	 */
	public function remap( $postarr, $item ) {
		
		

		return $item;
	}
	
}