<?php
/**
*
* Abstract post remap handler
*
* Render form field for remap
* Store user chose
* Hook into post import process and remap target attribute
*
*/

class RSSI_Map_Xml_Field {

	/**
	 * Field Text label
	 * @var string
	 */
	private $label;

	/**
	 * Field name used in form and in db storage
	 * @var string
	 */
	private $name;

	/**
	 * Store Processing campaign id
	 * @var int
	 */
	private $campaign_id;

	/**
	 * Add form render and save filters + remap filter 
	 */
	function __construct() {}

	/**
	 * Get field form field
	 * @return string
	 */
	public function get_field() {
		
	}

	/**
	 * Save user field choise
	 * @return void
	 */
	public function save() {

	}

	/**
	 * Get selected field value from db
	 * @return array|bool 
	 */
	public function get_val(){
		
	}

	/**
	 * Process post attribute remap
	 * This function may be run before post import process and change postarr passed to wp_insert post
	 * of after successfull post import 
	 * 
	 * @return mixed
	 */
	public function remap() {}

}