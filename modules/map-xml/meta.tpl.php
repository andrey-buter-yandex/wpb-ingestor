<?php 
global $post;

$hide = ' style="display:none"';
?>
<div class="rss-import rssi-map-xml">

	<?php
		$enable = get_campaign_meta( $post->ID, 'usexmlmap', true );
	?>
	<div class="rssi-row">
		<label for="rssi-manual-xml-map"><?php _e( 'Map manually', 'rssi' ); ?></label>
		<input type="checkbox" name="rssi[xmlmap][usexmlmap]" id="rssi-manual-xml-map" value="1" <?php checked( '1', $enable ); ?> />
	</div>

	<div class="rssi-manual-mapping" <?php if ( empty( $enable ) ) echo $hide; ?>>

		<div id="rssi-map-updates-nodes-errors"></div>

		<div class="rssi-row label-row">
			<label class="main-label" for="">Update tags list <span id="rssi-map-xml-update-nodes-spinner" class="spinner" style="display: none;"></span></label>
			<button class="button" id="rssi-map-xml-update-nodes"><?php _e( 'Refresh', 'rssi' ); ?></button>
		</div>

		<?php

			// Custom manual xmlmap
			$map = get_campaign_meta( $post->ID, 'xmlmap', true );
			$map = empty( $map ) ? array() : $map;

			// Retrieved and saved available feed xml nodes
			$tags = get_campaign_meta( $post->ID, 'xmlmap_tags', true );
		?>
		<div class="rssi-manual-mapping-fields" <?php if ( empty( $tags ) ) echo $hide; ?> >

			<?php foreach ( rssi_xmlmap_get_fields() as $key => $title ): 
				$current = isset( $map[ $key ] ) ? $map[ $key ] : null;
			?>

			<div class="rssi-row label-row">
				<label class="main-label" for="rssi-manual-xml-map-<?php echo $key ?>"><?php echo $title; ?></label>
				<select class="js-map-field" id="rssi-manual-xml-map-<?php echo $key ?>" name="rssi[xmlmap][xmlmap][<?php echo $key ?>]">
					<?php echo rssi_xmlmap_get_tags_options( $tags, $current ); ?>
				</select>
			</div>
			<?php endforeach ?>

			<?php 
				$first_checked = get_campaign_meta( $post->ID, 'recurrence', true ) ? '' : 'checked';
				$map_meta = get_campaign_meta( $post->ID, 'xmlmap-simple-meta', true );
				$key      = 'post_append_thumbnail';
			?>
			<div class="rssi-row label-row">
				<label style="line-height: 16px" class="main-label" for="rssi-manual-xml-map-meta-<?php echo $key ?>"><?php _e( 'Show Featured Image on Post Page' ) ?></label>
				<input <?php echo $first_checked ?> type="checkbox" value="1" class="js-map-field" id="rssi-manual-xml-map-meta-<?php echo $key ?>" name="rssi[xmlmap-simple-meta][<?php echo $key ?>]" <?php checked( '1', $map_meta[$key] ) ?>>
			</div>

		</div>
	</div>
</div>