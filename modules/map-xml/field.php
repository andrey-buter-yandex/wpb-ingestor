<?php
/**
*
* Manage campaign xml mapping data
* - render meta box
* - store xml mappging data as campaign meta
* - handle ajax request for available xml nodes
* 
*
*/

class RSSI_Map_Xml_Field {

	/**
	 * Field Text label
	 * @var string
	 */
	private $label;

	/**
	 * Field name used in form and in db storage
	 * @var string
	 */
	private $name;

	/**
	 * Store Processing campaign id
	 * @var int
	 */
	private $campaign_id;


	function __construct() {
		global $wpdb, $pagenow;

		// Hook into campaign import process
		//add_action( 'rssi_remap_import' );
	}


	function process($value='')
	{
		# code...
	}
	
}