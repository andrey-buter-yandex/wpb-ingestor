<?php
/**
* Handle xml nodes mapping
*
* Provide ui to connect feed item fields with post fields
* Hook into importing process to change default mapping with custom if needed
*
*
* @todo  To allow multiple campaign handling in one request move object creation to function called on campaign import action
*/

class RSSI_Map_Xml_Class {

	/**
	 * Store Processing campaign id
	 * @var int
	 */
	private $campaign_id;

	function __construct() {
		add_action( 'rssi_import_campaign', array( &$this, 'remap_campaign' ), 10, 2 );
	}

	/**
	 * Maybe process feed remap
	 * @param  [type] $postarr [description]
	 * @param  [type] $item    [description]
	 * @return [type]          [description]
	 */
	public function remap_campaign( $campaign_id ) {
		
		// If current campaign has valid remap array add remap filter
		if ( get_campaign_meta( $campaign_id, 'usexmlmap' ) ) {

			$this->campaign_id = $campaign_id;

			add_filter( 'rssi_import_post_data', array( &$this, 'remap_item' ), 10, 2 );

			add_action( 'rssi_import_post', array( &$this, 'remap_meta' ), 10, 2 );
		}
	}

	/**
	 * Handle manual tags mappging during campaign import process
	 * @param  array  $postarr Post attributes array
	 * @param object $item     SimpliPie Item Object
	 * @return array           Filtered post attributes
	 * 
	 *
	 * @todo each fields class must handle this manually
	 */
	function remap_item( $postarr, $item ) {

		$map = get_campaign_meta( $this->campaign_id, 'xmlmap', true );

		if ( !empty( $map ) ) {

			if ( isset( $map['post_title'] ) && $data = $item->get_item_tags( $map['post_title'][0], $map['post_title'][1] ) ) {
				$postarr['post_title'] = $item->sanitize($data[0]['data'], SIMPLEPIE_CONSTRUCT_TEXT);
			}

			if ( isset( $map['post_content'] ) && $data = $item->get_item_tags( $map['post_content'][0], $map['post_content'][1] ) ) {
				$postarr['post_content'] = $item->sanitize($data[0]['data'], SIMPLEPIE_CONSTRUCT_HTML);
			}
		}

		return $postarr;
	}

	/**
	 * Update simple map meta
	 * now - show thumbnail attr
	 */
	public function update_map_simple_meta( $post_id )
	{
		$simple_meta = get_campaign_meta( $this->campaign_id, 'xmlmap-simple-meta', true );

		if ( !$simple_meta )
			return;

		foreach ( $simple_meta as $key => $value )
			update_post_meta( $post_id, $key, $value );
	}

	/**
	 * 
	 */
	public function remap_meta( $post_id, $item ) {

		$map = get_campaign_meta( $this->campaign_id, 'xmlmap', true );

		if ( !empty( $map ) ) {

			if ( isset( $map['meta-url'] ) && $data = $item->get_item_tags( $map['meta-url'][0], $map['meta-url'][1] ) ) {
				$url = $item->sanitize($data[0]['data'], SIMPLEPIE_CONSTRUCT_TEXT);
				update_post_meta( $post_id, 'post_url', $url );
			}

			if ( isset( $map['thumbnail'] ) && $data = $item->get_item_tags( $map['thumbnail'][0], $map['thumbnail'][1] ) ) {
				$data = $data[0];
				$url = '';

				// Preferably get from attributes
				if ( isset( $data['attribs'] ) ) {

					// deal only with top levelt attrs
					$attribs = array_shift( $data['attribs'] );

					if ( isset( $attribs['url'] ) )
						$url = $attribs['url'];
				}

				$url = $item->sanitize( $url, SIMPLEPIE_CONSTRUCT_TEXT );

				if ( !empty( $url ) )
					$this->attach_thumbnail( $url, $post_id );
			}
		}

		// update simple map meta
		$this->update_map_simple_meta( $post_id );

	}

	/**
	 * Attach thumbnail to the post
	 * @param  string $url     Thumbnail url
	 * @param  int $post_id    Post ID
	 * @return void
	 */
	public function attach_thumbnail( $image_url, $post_id ) {

		$filename = basename($image_url);
		$upload = wp_upload_bits($filename, null, file_get_contents($image_url));

		if ( isset( $upload['error'] ) && $upload['error'] !== false )
			return;


		// $filename should be the path to a file in the upload directory.
		$filename = $upload['file'];

		// Check the type of tile. We'll use this as the 'post_mime_type'.
		$filetype = wp_check_filetype( basename( $filename ), null );

		// Get the path to the upload directory.
		$wp_upload_dir = wp_upload_dir();

		// Prepare an array of post data for the attachment.
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ), 
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);

		// Insert the attachment.
		$attach_id = wp_insert_attachment( $attachment, $filename, $post_id );

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
		wp_update_attachment_metadata( $attach_id, $attach_data );

		set_post_thumbnail( $post_id, $attach_id );
	}

}
new RSSI_Map_Xml_Class;
