<?php
/**
* Intialize adv meta functions
*/

class XML_Map_Simple_Meta {

	public $post_type = '';
	public $map_meta_key = 'xmlmap-simple-meta';

	function __construct() {
		if ( is_admin() ) {
			$this->post_type = rssi_get_campaign_post_type();
			$this->meta_keys = array(
				'show_thumbnail',
			);
			add_action( 'init', array( &$this, 'init_meta_box' ) );
		}
	}

	public function init_meta_box() {
		global $pagenow;

		if ( !(  ( defined('DOING_AJAX') && DOING_AJAX ) || in_array( $pagenow, array( 'post.php', 'post-new.php' ) ) ) ) 
			return;

		// Handle campaign term options storage
		add_action( 'save_post',   array( &$this, 'save_meta'   ) );
		add_action( 'delete_post', array( &$this, 'delete_meta' ) );
	}

	/**
	 * Save current page meta
	 * @param  int $post_id Current post ID
	 * @return void
	 */
	function save_meta( $post_id ) {
		if ( !$_POST )
			return;

		$post = get_post( $post_id );

		if ( $this->post_type !== $post->post_type )
			return;

		if ( !isset( $_POST['rssi'][$this->map_meta_key] ) )
			return delete_campaign_meta( $post_id, $this->map_meta_key );

		$data = array_map( 'stripslashes_deep', $_POST['rssi'][$this->map_meta_key] );

		update_campaign_meta( $post_id, $this->map_meta_key, $data );
	}

	/**
	 * Delete campaign term meta on campaign removal
	 * @param  int $post_id Campaign id
	 * @return void
	 */
	function delete_meta( $post_id ) {
		foreach ( $this->meta_keys as $key ) {
			delete_campaign_meta( $post_id, $key );
		}
	}

}
if ( is_admin() )
	new XML_Map_Simple_Meta;