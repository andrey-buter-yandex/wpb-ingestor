<?php
/**
*
* Manage campaign xml mapping data
* - render meta box
* - store xml mappging data as campaign meta
* - handle ajax request for available xml nodes
* 
*
*/

class RSSI_Map_Xml_Meta {

	/**
	 * Store Processing campaign id
	 * @var int
	 */
	private $campaign_id;

	function __construct() {
		if ( is_admin() ) {
			add_action( 'init', array( &$this, 'init_meta' ) );

			// Listen to ajax xml nodes request
			add_action( 'wp_ajax_rssi_xmltags', array( &$this, 'respond_tags' ) );			
		}
	}

	public function init_meta() {
		global $pagenow;

		if ( !( defined('DOING_AJAX') && DOING_AJAX || in_array( $pagenow, array( 'post.php', 'post-new.php' )) ) )
			return;

		// Load meta boxes
		add_action( 'admin_menu', array( &$this, 'add_meta_box' ) );

		// Include specific scripts and styles
		add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_scripts' ) );

		// Handle campaign term options storage
		add_action( 'save_post', array( &$this, 'save_meta' ) );
		add_action( 'delete_post', array( &$this, 'delete_meta' ) );
	}

	/**
	 * Add adv main meta box
	 */
	function add_meta_box() {
		add_meta_box( 'rssi_map_xml', __( 'Custom XML mapping', 'rssi' ), array( &$this, 'show_meta_box' ), rssi_get_campaign_post_type(), 'normal' );
	}

	/**
	 * Show meta box content
	 * @param  object $post Current post object
	 * @return void
	 */
	function show_meta_box( $post ) {
		global $rssi_map_xml;

		require( RSSI_ABSPATH . $rssi_map_xml->include_path . 'meta.tpl.php' );
	}

	/**
	 * Save current page meta
	 * @param  int $post_id Current post ID
	 * @return void
	 */
	function save_meta( $post_id ) {
		if ( !isset( $_POST ) || !isset( $_POST['post_type'] ) || rssi_get_campaign_post_type() !== $_POST['post_type'] )
			return;

		// Check if data presents
		if ( !isset( $_POST['rssi']['xmlmap'] ) )
			return;

		$data = isset( $_POST['rssi']['xmlmap'] ) ? $_POST['rssi']['xmlmap'] : array();
		$this->update_meta( $post_id, $data );
	}

	/**
	 * Update xml-map campaign meta
	 * @param  int $post_id Post id (Campaign)
	 * @param  array $data  Form data
	 * @return void
	 */
	private function update_meta( $post_id, $data ) {

		// Update enable checkbox
		if ( isset( $data['usexmlmap'] ) ) {
			update_campaign_meta( $post_id, 'usexmlmap', 1 );
		} else {
			delete_campaign_meta( $post_id, 'usexmlmap' );
		}

		// In html form all fields values are stored as array numeral keys 'namespace_key'-'tag-key'
		// But in database we will need to store namespace and tags itself
		$map = $this->convert_map_to_db( $post_id, $data['xmlmap'] );

		if ( !empty( $map ) ) {
			update_campaign_meta( $post_id, 'xmlmap', array_map( 'stripslashes_deep', $map ) );
		} else {
			delete_campaign_meta( $post_id, 'xmlmap' );
		}

	}

	/**
	 * Delete current page meta
	 * @param  int $post_id Banne id
	 * @return void
	 */
	function delete_meta( $post_id ) {
		global $wpdb;
		$wpdb->delete( $wpdb->bannerdisplay, array( 'banner_id' => $post_id ) );
	}

	/**
	 * Register and enqueue xmlmap meta box scripts
	 * @return void
	 */
	function enqueue_scripts() {
		global $typenow, $rssi_map_xml;
		if( rssi_get_campaign_post_type() == $typenow ) {

			// Registers and enqueues the required javascript.
			wp_register_script( 'rssi_xmlmap', plugins_url( $rssi_map_xml->include_path . 'assets/js/xmlmap.js', RSSI_FILE ), array( 'jquery' ) );
			wp_enqueue_script( 'rssi_xmlmap' );

		}
	}

	/**
	 * Respond with array of available nodes for requested url
	 * Is run as ajax callback
	 * @return void Output json and exit
	 */
	public function respond_tags() {
		
		// Validate passed params
		if ( !isset( $_GET['url'] ) ) {
			exit( json_encode( array( 'error' => 'Empty url provided' ) ) );
		}

		// Create fast feed object
		$feed = new RSSI_Feed();
		$feed->set_url( $_GET['url'] );
		$feed->set_stupidly_fast( true );
		$item = $feed->get_items( 0, 1 );

		if ( is_wp_error( $item ) ) {
			exit( json_encode( array( 'error' => $item->get_error_message() ) ) );
		}

		// We have valid feed item get all avaialable xml tags with their namespaces
		$item = array_shift( $item );
		$tags = $this->parse_tags( $item->data['child'] );

		// Update campaign xmlmap meta
		if ( isset( $_GET['campaign_id'] ) ) {
			update_campaign_meta( absint( $_GET['campaign_id'] ), 'xmlmap_tags', $tags );
		}

		exit( json_encode( array( 'options' => rssi_xmlmap_get_tags_options( $tags ) ) ) );
	}

	/**
	 * Get all available tags grouped by namespace
	 * @param  array $child SimplePie data child item
	 * @return array
	 */
	function parse_tags( $child ) {
		$options = array();

		if ( !is_array( $child ) || empty( $child ) ) {
			return array();
		}

		$tags_options = array();

		// Collect only namespaces with all available tags inside
		foreach ( $child as $namespace => $tags ) {
			$options[] = array(
				'key' => $namespace,
				'tags' => array_keys( $tags )
			);
		}

		return $options;
	}

	/**
	 * In html form all fields values are stored as array numeral keys 'namespace_key'-'tag-key'
	 * but in database we will need to store namespace and tags itself
	 * 
	 * @param  array $map POST format map keys
	 * @return array      Converted map
	 */
	private function convert_map_to_db( $campaign_id, $post_map = array() ) {

		// To convert map we will need available xml map
		$tags = get_campaign_meta( $campaign_id, 'xmlmap_tags', true );

		if ( empty( $tags ) || empty( $post_map ) ) {
			return null;
		}

		// Converted map
		$map = array();

		foreach ( $post_map as $field_key => $indexes ) {
			$indexes = explode( '-', $indexes );

			// Indexes store numeral keys divided by '-'
			if ( is_array( $indexes ) && count( $indexes ) == 2 ) {
				$indexes = array_map( 'absint', $indexes );

				// Extract namespace and tag according to provided indexes
				if ( isset( $tags[ $indexes[0] ] ) && isset( $tags[ $indexes[0] ]['tags'][ $indexes[1] ] ) ) {
					$map[ $field_key ] = array(
						$tags[ $indexes[0] ]['key'],
						$tags[ $indexes[0] ]['tags'][ $indexes[1] ]
					);
				}
			}
		}

		return $map;
	}
}
new RSSI_Map_Xml_Meta;