/**
 * Handle interaction with xmlmeta box for campaigns
 * We assume that this script is loaded only when meta-box presents on the page
 */
"use strict";

jQuery(document).ready(function($) {

	// Dom cache
	var toggler = $('#rssi-manual-xml-map'),
		mainContainer = $('.rssi-manual-mapping'),
		requestBtn = $('#rssi-map-xml-update-nodes'),
		spinner = $('#rssi-map-xml-update-nodes-spinner'),
		fieldsContainer = $('.rssi-manual-mapping-fields'),
		mapFields = fieldsContainer.find('select.js-map-field'),
		errorsBox = $('#rssi-map-updates-nodes-errors');

	/**
	 * Hold all available xml tags
	 * @type {Array}
	 */
	var xmlTags = [];


	/**
	 * Show hide xmlmap box content
	 */
	toggler.on('change', function() {
		if ( toggler.attr('checked') ) {
			mainContainer.show();
		} else {
			mainContainer.hide();
		}
	});

	/**
	 * Request all available nodes on button click
	 */
	requestBtn.on('click', function(e) {
		e.preventDefault();

		requestXMLTags();
	});

	/**
	 * Request for all available xml tags for current url
	 */
	function requestXMLTags() {
		
		$.ajax({
			url: ajaxurl,
			dataType: 'json',
			data: {
				action: 'rssi_xmltags',
				url: $('#campaign_general_url').val(),
				campaign_id: $('#post_ID').val()
			},
			beforeSend: function() {
				spinner.show();
			}
		}).success(function(data) {

			// Clear errors
			errorsBox.html('');

			// Update map fields options
			if ( data.hasOwnProperty('options') ) {
				mapFields.html( data.options );

				fieldsContainer.show();
			}

			if ( data.hasOwnProperty('error') ) {
				errorsBox.html( '<p class="error">' + data.error + '</p>' );
			}
		}).done(function() {
			spinner.hide();
		});
	}

	/**
	 * Insert all available tags as select options
	 * @param {Array} options Tags with their namespaces with titles
	 */
	function setTagsOptions(options) {
		var htmlOpts = '';

		// Create options string
		for (option in options) {
			if (options.hasOwnProperty(option)) {
				htmlOpts += '<option value="' + option + '">' + options[option] + '</option>';
			}
		}

		mapFields.html(htmlOpts);
	}



});