<?php
/**
* Handle xml nodes mapping
*
* Provide ui to connect feed item fields with post fields
* Hook into importing process to change default mapping with custom if needed
*
*
* @todo Allow multiple campaign handling in one request, move object creation to function called on campaign import action
*/

class RSSI_Map_Xml {

	/**
	 * Store Processing campaign id
	 * @var int
	 */
	public $include_path = '/modules/map-xml/';

	/**
	 * Hold array of editable postarray fields
	 * @var array
	 */
	private $fields;

	function __construct() {

		// Load submodules		
		$this->load_parts();
	}

	/**
	 * Load xml map partials
	 * @return void
	 */
	private function load_parts() {

		require_once( RSSI_ABSPATH . $this->include_path . 'map-xml-class.php' );

		// Public methods
		require_once( RSSI_ABSPATH . $this->include_path . 'public.php' );

		// Handle xml map meta
		require_once( RSSI_ABSPATH . $this->include_path . 'meta.php' );

		// Xml map handler class
		require_once( RSSI_ABSPATH . $this->include_path . 'meta.php' );

		// Xml map handler class
		require_once( RSSI_ABSPATH . $this->include_path . '/inc/simple-meta.php' );
	}

	/**
	 * Get array of available editable fields
	 * 
	 * @return array
	 * 
	 * @todo move each field to separate file
	 * with each field handler
	 */
	function get_fields() {
		if ( isset( $this->fields ) )
			return $this->fields;

		$this->fields = apply_filters( 'rssi_map_xml_fields', array(
			'post_title'   => __( 'Post Title', 'rssi' ),
			'post_content' => __( 'Post Content', 'rssi' ),
			'thumbnail'    => __( 'Thumbnail', 'rssi' ),
			'meta-url'     => __( 'External Url', 'rssi' )
		));

		return $this->fields;
	}

}

/**
 * Load Map class after main plugin class has loaded
 */
add_action( 'rssi_after_setup', 'load_rssi_map_xml' );
function load_rssi_map_xml(  ) {
	$GLOBALS['rssi_map_xml'] = new RSSI_Map_Xml;
}