<?php
/**
*
* Manage campaign xml mapping data
* - render meta box
* - store xml mappging data as campaign meta
* - handle ajax request for available xml nodes
* 
*
*/


function rssi_xmlmap_get_fields() {
	global $rssi_map_xml;
	return $rssi_map_xml->get_fields();
}


/**
 * Build html options array 
 * @param  [type] $current [description]
 * @param  [type] $tags    [description]
 * @return [type]          [description]
 *
 * @todo  Use meta object method for this
 */
function rssi_xmlmap_get_tags_options( $tags, $current = array() ) {
	global $rssi_map_xml_meta;

	$options = '';

	if ( empty( $tags ) )
		return $options;

	// Current attribute must contain array with 2 elements, 'namespace' and 'tag', extract them from $current
	$cur_namespace = isset( $current[0] ) ? $current[0] : '';
	$cur_tag = isset( $current[1] ) ? $current[1] : '';

	$options .= '<option value="">Leave empty</option>';

	foreach ( $tags as $group_key => $values ) {
		foreach ( $values['tags'] as $value_key => $value ) {
			$val = $group_key . '-' . $value_key;

			// Check current value
			$current = ( $values['key'] === $cur_namespace && $value === $cur_tag );

			$options .= '<option value="'. $val .'" '. selected( true, $current, false ) .'>'. $value .'</option>';
		}
	}

	return $options;
}