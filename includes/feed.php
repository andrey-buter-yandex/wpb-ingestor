<?php
/**
* Handle single feed resource import 
* Provide filters and actions for plugin modules
*
* Feed class also handles low level importing of feed items into posts
* 
* 
*/
class RSSI_Feed {

	/**
	 * Hold feed url
	 * @since  1.0
	 * @access protected
	 * 
	 * @var string
	 */
	protected $url;

	/**
	 * Set options to make SimplePie as fast as possible
	 * @since  1.0
	 * @access protected
	 * 
	 * @var bool
	 */
	protected $stupidly_fast = false;

	/**
	 * Hold feed object
	 * @var object SimplePie
	 */
	protected $feed;

	/**
	 * Hold errors if they occure
	 * @var object WP_Error
	 */
	protected $errors;


	/**
	 * Return feed url
	 * @return string|bool Return either feed url or false if none isset
	 */
	public function get_url() {
		if ( isset( $this->url ) )
			return $this->url;
		return false;
	}

	/**
	 * Set url if it is valid
	 * @param string $url Feed url
	 */
	public function set_url( $url = '' ) {
		$error = $this->valid_url( $url );
		if ( !is_wp_error( $error ) )
			$this->url = $url;

		return $error;
	}

	/**
	 * Check if url is 
	 * @param  string  $url  Url to validate
	 * @return (bool|object) Return true if url is valid or WP_Error if url is invalid
	 */
	public function valid_url( $url = '' ) {
		$error = new WP_Error();

		if ( empty( $url ) )
			$error->add( 'empty_url', __( 'The url passed to the feed is empty' ) );

		if ( $error->get_error_codes() )
			return $error;

		return true;
	}

	/**
	 * Set stupidly_fast SimplePie argument
	 * @param boolean $stupidly_fast stupidly_fast attribute value
	 */
	public function set_stupidly_fast( $stupidly_fast = true ) {
		$this->stupidly_fast = $stupidly_fast;
	}

	/**
	 * Get SimplePie 
	 * @return Object Will return either SimplePie object or WP_Error object
	 */
	function get_feed() {
		if ( isset( $this->feed ) )
			return $this->feed;

		if ( isset( $this->errors ) )
			return $this->errors;

		// Create new rss feed
		require_once ( ABSPATH . WPINC . '/class-feed.php' );

		$feed = new SimplePie();

		$feed->set_sanitize_class( 'WP_SimplePie_Sanitize_KSES' );

		// We must manually overwrite $feed->sanitize because SimplePie's
		// constructor sets it before we have a chance to set the sanitization class
		$feed->sanitize = new WP_SimplePie_Sanitize_KSES();

		$feed->set_file_class( 'WP_SimplePie_File' );
		$feed->set_feed_url( $this->url );
		$feed->set_stupidly_fast( $this->stupidly_fast );

		// No need to cache results
		$feed->enable_cache( false );

		$feed->init();
		$feed->handle_content_type();

		if ( $feed->error() ) {
			return $this->add_error( 'simplepie-error', $feed->error() );
		}

		// Store SimplePie feed object
		$this->feed = $feed;

		return $this->feed;
	}

	/**
	 * Get feed items
	 * @param  int $page  Feed page to start with
	 * @param  int $limit Maximum number of items to return
	 * @return (array|object)  Array of 
	 */
	public function get_items( $page, $limit ) {
		$feed = $this->get_feed();

		if ( is_wp_error( $feed ) ) {
			return $feed;
		}

		$maxitems = $feed->get_item_quantity( $limit );

		if ( empty( $maxitems ) )
			return new WP_Error( 'empty-feed', 'No items in the feed' );

		// Calculate offset for specified page
		$offset = $page * $limit;

		if ( $offset > $maxitems )
			return new WP_Error( 'no-more-items', 'No more items in the feed' );

		// Build an array of all the items, starting with element 0 (first element).
    	return $feed->get_items( $offset, $maxitems );
	}

	/**
	 * Add new error code
	 * @param string $code    Error Code
	 * @param string $message Error message
	 */
	protected function add_error( $code, $message ) {
		if ( !isset( $this->errors ) ) {
			$this->errors = new WP_Error();
		}
		$this->errors->add( $code, $message );
		return $this->errors;
	}

	/**
	 * Setup some attributes
	 *
	 * @todo Parse and set arguments on object creation
	 */
	public function __construct( $args = array() ) {
		if ( !empty( $args ) ) {

		}
	}
}