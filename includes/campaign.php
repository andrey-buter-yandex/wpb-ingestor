<?php
/**
* Campaign initilize class
* Handle submodules load
* 
* Campaigns hold all data about the importing feed
* 
* 
*/
class RSSI_Campaign {

	/**
	 * Hold submodules include path
	 * @since  2.0
	 * @access private
	 * 
	 * @var string
	 */
	private $include_path;

	/**
	 * Hold post_type name accesibled to whole application
	 * @var string
	 */
	public $post_type = 'rssi_campaign';

	/**
	 * Make this object accessible to its descendants
	 * @todo convert to singletone
	 */
	function __construct() {

		$GLOBALS['rssi_campaign'] = & $this;

		// Path to submodules
		$this->include_path = RSSI_ABSPATH . '/includes/campaign/';

		// Load dependant submodules
		$this->load_modules();
	}

	/**
	 * Include all dependant modules
	 * @since 2.0
	 * @access private
	 *
	 * @return void
	 */
	function load_modules() {

		// Campaign public methods
		require_once( $this->include_path . 'public.php' );

		// Define Campaign post type
		require_once( $this->include_path . 'model.php' );

		// Meta box module
		require_once( $this->include_path . 'meta.php' );

		// Import handler
		require_once( $this->include_path . 'import.php' );
	}

}

// Initialize this module during plugin setup
add_action( 'rssi_after_setup', 'load_rss_import_campaign' );
function load_rss_import_campaign() {
	new RSSI_Campaign;
}