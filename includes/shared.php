<?php
/**
* Handle advertisement meta box form render
* and meta save, 
* also provides public actions and filters for addtional features
*/
class RSSI_Shared {

	/**
	 * Hold path to shared classes dir
	 * @var string
	 */
	private $include_path;

	function __construct() {

		$this->include_path = RSSI_ABSPATH . '/includes/shared/';

		// Plugin options class
		require_once( $this->include_path . 'class-plugin-options.php' );

		// Plugin template class
		require_once( $this->include_path . 'class-plugin-view.php' );
	}

}

// Initialize shared class before plugin loads
add_action( 'rssi_before_setup', 'load_rss_import_shared_class' );
function load_rss_import_shared_class() {
	new RSSI_Shared;
}