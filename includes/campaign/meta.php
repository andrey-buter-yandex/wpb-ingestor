<?php
/**
* Intialize adv meta functions
*/

class Rss_Import_Campaign_Meta {

	function __construct() {
		global $wpdb;

		// Add bannermeta table to global wpdb object
		$wpdb->campaignmeta = $wpdb->prefix . 'campaignmeta';

		if ( is_admin() ) {
			add_action( 'init', array( &$this, 'init_meta' ) );

			// Check or create meta table on plugin activation
			// Multisite bug
			// register_activation_hook( RSSI_FILE, array( $this, 'check_meta_table' ) );
			if ( !( defined('DOING_AJAX') && DOING_AJAX ) )
				add_action( 'init', array( $this, 'check_meta_table' ) );
		}
	}

	public function init_meta(){
		global $pagenow;

		if ( !( defined('DOING_AJAX') && DOING_AJAX || in_array( $pagenow, array( 'post.php', 'post-new.php' )) ) )
			return;

		// Enqueue meta scripts
		add_action( 'admin_enqueue_scripts', array( &$this, 'enqueue_scripts' ) );

		// Load meta boxes
		add_action( 'admin_menu', array( &$this, 'add_meta_box' ) );

		// Meta box save action
		add_action( 'save_post', array( &$this, 'save_meta_box' ) );
	}

	/**
	 * Add adv main meta box
	 */
	function add_meta_box() {
		add_meta_box( 'advertise_meta', __( 'Attributes', 'rssi' ), array( &$this, 'show_meta_box' ), rssi_get_campaign_post_type(), 'normal', 'high' );
	}

	/**
	 * Show meta box content
	 * @param  object $post Current post object
	 * @return void
	 */
	function show_meta_box( $post ) {
		require( RSSI_ABSPATH . '/includes/campaign/meta.tpl.php' );
	}

	/**
	 * Save advertise meta
	 * @param  int $post_id Post id
	 * @return void
	 */
	function save_meta_box( $post_id ) {

		if ( !isset( $_POST['post_type'] ) || rssi_get_campaign_post_type() !== $_POST['post_type'] ) {
			return;
		}

		// Update general banner meta
		$this->save_general_meta( $post_id );
	}

	/**
	 * General banner meta includes vital banner data
	 * including banner source code, banner media location (src for images or embed tags), background
	 * @param  int $post_id Current campaign ID
	 * @return void
	 */
	function save_general_meta( $post_id ) {
		global $wpdb;

		if ( !isset( $_POST['rssi']['general'] ) )
			return;

		$data = array_map( 'stripslashes_deep', $_POST['rssi']['general'] );

		$data = wp_parse_args( $data, array(
			'url'          => '',
			'limit'        => '10',
			'recurrence'   => 'daily',
			'init_limit'   => '10',
			'post_author'  => '1',
			'post_status'  => '',
			'target_blank' => ''
		));

		foreach ($data as $key => $value) {
			if ( !empty( $value ) ) {
				update_campaign_meta( $post_id, $key, $value );
			} else {
				delete_campaign_meta( $post_id, $key );
			}
		}
	}

	/**
	 * Register and enqueue meta box scripts
	 * @return void
	 */
	function enqueue_scripts() {
		global $typenow;
		if( rssi_get_campaign_post_type() == $typenow ) {
			wp_enqueue_media();

			// Registers and enqueues the required javascript.
			wp_register_script( 'campaign-meta', plugins_url( 'assets/js/campaign-meta.js', RSSI_FILE ), array( 'jquery' ) );
			//wp_enqueue_script( 'campaign-meta' );

			wp_register_style( 'rssi', plugins_url( 'assets/css/screen.css', RSSI_FILE ) );
			wp_enqueue_style( 'rssi' );
		}
	}

	/**
	 * Create separate table to save advertise meta
	 * 
	 */
	function check_meta_table() {
		global $wpdb;

		if ( !$wpdb->query( "SHOW TABLES LIKE '$wpdb->campaignmeta'" ) ) {

			if ( ! empty($wpdb->charset) )
				$charset_collate = "DEFAULT CHARACTER SET $wpdb->charset";
			if ( ! empty($wpdb->collate) )
				$charset_collate .= " COLLATE $wpdb->collate";

			$query = "CREATE TABLE $wpdb->campaignmeta (
					  meta_id bigint(20) unsigned NOT NULL auto_increment,
					  campaign_id bigint(20) unsigned NOT NULL default '0',
					  meta_key varchar(255) default NULL,
					  meta_value longtext,
					  PRIMARY KEY  (meta_id),
					  KEY campaign_id (campaign_id),
					  KEY meta_key (meta_key)
					) $charset_collate;";

			$wpdb->query( $query );
		}
	}

}
new Rss_Import_Campaign_Meta;