<?php 
global $post;
?>
<div class="rss-import-meta rss-import" id="js-rss-import-meta">

	<div class="rssi-form">

		<div class="rssi-row label-row">
			<label for="campaign_general_url" class="main-label">Feed Url</label>
			<input placeholder="http://" type="text" name="rssi[general][url]" id="campaign_general_url" value="<?php echo esc_attr( get_campaign_meta( $post->ID, 'url', true ) ) ?>" class="widefat">
		</div>

		<div class="rssi-row label-row with-description">
			<label for="campaign_general_frequency_day" class="main-label">Frequency</label>

			<?php 
				$schedules = wp_get_schedules();
				$recurrence = get_campaign_meta( $post->ID, 'recurrence', true );
			?>
			<select name="rssi[general][recurrence]">
				<?php foreach ($schedules as $key => $interval ): ?>
					<option value="<?php echo $key; ?>" <?php selected( $key, $recurrence ) ?>><?php echo $interval['display'] ?></option>
				<?php endforeach ?>
			</select>

			<p class="description">How often should feeds be checked?</p>
		</div>

		<div class="rssi-row label-row">
			<label for="campaign_general_limit" class="main-label">Limit per request</label>
			<input type="text" name="rssi[general][limit]" id="campaign_general_limit" value="<?php echo esc_attr( get_campaign_meta( $post->ID, 'limit', true ) ) ?>" length="2">
		</div>

		<div class="rssi-row label-row">
			<label for="campaign_general_init_limit" class="main-label">Initial Limit</label>
			<input type="text" name="rssi[general][init_limit]" id="campaign_general_init_limit" value="<?php echo esc_attr( get_campaign_meta( $post->ID, 'init_limit', true ) ) ?>" length="2">
			<p class="description">Initial campaign import limit</p>
		</div>

		<div class="rssi-row label-row with-description">
			<label for="campaign_general_url" class="main-label">Author</label>
			<?php wp_dropdown_users(array(
				'selected' => get_campaign_meta( $post->ID, 'post_author', true ),
				'name' => 'rssi[general][post_author]',
				'who' => 'authors'
			)); ?>
			<p class="description">The created posts will be assigned to this author.</p>
		</div>

		<div class="rssi-row label-row with-description">
			<label for="campaign_general_frequency_day" class="main-label">Status</label>

			<?php $post_status = get_campaign_meta( $post->ID, 'post_status', true ); ?>
			<select name="rssi[general][post_status]">
				<option value="publish" <?php selected( 'publish', $post_status ) ?>>Publish</option>
				<option value="draft" <?php selected( 'draft', $post_status ) ?>>Draft</option>
			</select>
			<p class="description">All imported posts will have this status assigned.</p>
		</div>

		<?php $comment_status = get_campaign_meta( $post->ID, 'comment_status', true ); ?>
		<div class="rssi-row label-row">
			<label for="campaign_general_comment_status" class="main-label">Discussion</label>
			<select name="rssi[general][comment_status]" id="campaign_general_comment_status">
				<option value="open" <?php selected( $comment_status, 'open' ) ?>>Allow Comments</option>
				<option value="close" <?php selected( $comment_status, 'close' ) ?>>Disable Comments</option>
			</select>
		</div>

		<?php $target_blank = get_campaign_meta( $post->ID, 'target_blank', true ); ?>
		<div class="rssi-row label-row">
			<label for="campaign_general_target_blank" class="main-label">Target</label>
			<select name="rssi[general][target_blank]">
				<option value="same" <?php selected( 'same', $target_blank ) ?>>Same window</option>
				<option value="blank" <?php selected( 'blank', $target_blank ) ?>>New window</option>
			</select>
		</div>

	</div>

	<?php wp_nonce_field( RSSI_DIRNAME, 'rss_import_meta' ); ?>
</div>