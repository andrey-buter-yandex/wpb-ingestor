<?php
/**
* Compaign import handler class
*
* This class schedules import job on campaign save action
* Also provide functions for the first time import
*
* Handle campaign import process
*/

class RSSI_Campaign_Import {

	/**
	 * Function which is called, when cron is run.
	 * @var string
	 */
	private $hook = 'rssi_campaign_cron';

	function __construct() {

		// Add cron callback listner
		if ( defined( 'DOING_CRON' ) && DOING_CRON ) {
			add_action( $this->hook, array( &$this, 'process' ) );			
		}

		if ( is_admin() ) {

			// Add schedule event on campaign save action
			add_action( 'save_post', array( &$this, 'schedule' ), 11 );

			// Unschedule post when moving to trash
			add_action( 'transition_post_status', array( &$this, 'maybe_unschedule' ), 10, 3 );	

			// Save post meta after insert post
			add_action( 'rssi_import_post', array( &$this, 'save_post_meta' ), 10, 3 );
		}

	}

	/**
	 * Schedule or reschedule cron task that will schedule campaign import
	 * Callback on save_post action 
	 *
	 * @param  int $post_id Current post id (campaign)
	 * @return void
	 */
	public function schedule( $post_id ) {
		$post = get_post( $post_id );

		if ( !isset( $_POST['post_type'] ) || rssi_get_campaign_post_type() !== $post->post_type ) {
			return;
		}

		// This action is fired only once on post save action
		remove_action( 'save_post', array( &$this, 'schedule' ) );

		// Process initial import on campaign creation
		if ( $limit = get_campaign_meta( $post_id, 'init_limit', true ) ) {
			$this->process( $post_id, $limit );
		}

		$args = array( 'campaign_id' => $post_id );
		$recurrence = get_campaign_meta( $post_id, 'recurrence', true );
		$schedule = wp_get_schedule( $this->hook, $args );

		// Check if recurrence interval has changed
		if ( !empty( $recurrence ) && $recurrence !== $schedule ) {

			// If previous event exists unschedule it
			if ( !empty( $schedule ) ) {
				wp_unschedule_event( wp_next_scheduled( $this->hook, $args ), $this->hook, $args );
			}

			wp_schedule_event( strtotime( '+10 seconds' ), $recurrence, $this->hook, $args );
		}
	}

	/**
	 * Single Campaign import cron job handler
	 *
	 * Page and limit arguments can be filterd on cron schedule
	 *
	 * @param int $campaign_id  Id of campaign to process
	 * @param int $page         Optional Page number to process
	 * @param int $limit        Optional number of items to process
	 * @return void
	 */
	public function process( $campaign_id = 0, $limit = 0, $page = 0 ) {
		$campaign = get_post( absint( $campaign_id ) );

		if ( empty( $campaign ) ) {

			// Log that user has requested imvalid campaign to process
			return;
		}

		// Let modules add filters and other stuff
		do_action( 'rssi_import_campaign', $campaign_id );

		// The url to retrieve rss from
		$feed_url = get_campaign_meta( $campaign->ID, 'url', true );

		// Setup feed handler object
		$feed = new RSSI_Feed();
		$feed = apply_filters( 'rssi_feed_class', $feed, $campaign_id );
		$feed->set_url( $feed_url );

		$limit = empty( $limit ) ? absint( get_campaign_meta( $campaign->ID, 'limit', true ) ) : absint( $limit );

		// Retrive items and allow modules to modify the result
		$items = apply_filters( 'rssi_campaign_import_items', $feed->get_items( $page, $limit ), $feed );

		if ( is_wp_error( $items ) ) {

			do_action( 'rssi_campaign_abort', $campaign_id, $items );

			// Log error
			return $items;
		}

		// Default post array attributes
		$postarr = apply_filters( 'rssi_import_post_default', $this->get_default_postarr( $campaign->ID ), $campaign->ID );

		// Decode special characters and some additional html
		add_action( 'rssi_import_post_data', array( $this, 'format_import_post_data' ), 100, 2 );

		add_filter( 'wp_kses_allowed_html', array( $this, 'allow_additional_html' ), 10, 2 );

		// Process post import
		foreach ( $items as &$item ) {
			$this->import_post( $item, $postarr, $campaign_id );
		}

		do_action( 'rssi_campaign_imported', $campaign_id );
	}

	/**
	 * Import single item into wp posts table
	 * @param  object $item    SimplePie Feed object
	 * @param  array  $postarr Default Post options
	 * @return int|oobject     Post id on success or WP_Error on failure
	 */
	private function import_post( $item, $postarr = array(), $campaign_id = 0 ) {

		// Allow plugins to hook item import
		do_action_ref_array( 'rssi_pre_import_post', array( &$item ) );

		if ( is_wp_error( $item ) ) {

			// Log error

			return;
		}

		// Retrieve essential attributes
		$postarr['post_title']   = $item->get_title();
		$postarr['post_content'] = $item->get_content();
		$postarr['post_date']    = $this->get_current_date( $item->get_date( 'U' ), 'Y-m-d H:i:s' );

		$postarr = apply_filters( 'rssi_import_post_data', $postarr, $item );

		$post_id = wp_insert_post( $postarr, true );

		if ( is_wp_error( $post_id ) ) {

			// Log error

			return;
		}

		do_action( 'rssi_import_post', $post_id, $item, $campaign_id );
	}

	/**
	 * Get date with gmt_offset
	 */
	private function get_current_date( $unixtime, $format = 'Y-m-d H:i:s' )
	{
		$unixtime = $unixtime + ( get_option( 'gmt_offset' ) * HOUR_IN_SECONDS );

		return date( $format, $unixtime );
	}

	/**
	 * Get default post attributes for current campaign
	 * @param  int $campaign_id Current campaign id
	 * @return array            Default post array variables
	 */
	private function get_default_postarr( $campaign_id ) {
		return array(
			'post_author'    => absint( get_campaign_meta( $campaign_id, 'post_author', true ) ),
			'post_status'    => get_campaign_meta( $campaign_id, 'post_status', true ),
			'comment_status' => get_campaign_meta( $campaign_id, 'comment_status', true ),
		);
	}

	/**
	 * Maybe unschedule campaign if it's desination status is not publish
	 * This is callback to transition_post_status hook
	 * @param  string $post_status     Target post status
	 * @param  string $previous_status Previous post status
	 * @param  object $post            WP_Post object which is campaign
	 * @return void
	 */
	public function maybe_unschedule( $post_status, $previous_status, $post ) {
		if ( rssi_get_campaign_post_type() == $post->post_type && 'publish' !== $post_type ) {
			$this->unschedule( $post->ID );
		}
	}

	/**
	 * Remove scheduled events for the campaign
	 * @param  int $post_id Current Campaign ID
	 * @return void         
	 */
	private function unschedule( $post_id ) {
		$args = array( 'campaign_id' => $post_id );
		$timestamp = wp_next_scheduled( $this->hook, $args );

		if ( false !== $timestamp ) {
			wp_unschedule_event( $timestamp, $this->hook, $args );
		}
	}

	/**
	 * Apply some campaign meta to imported posts
	 * @param  int $post_id     Current post ID
	 * @param  object $item     SimplePie object
	 * @param  int $campaign_id Current campaign ID
	 * @return void
	 */
	public function save_post_meta( $post_id, $item, $campaign_id ) {
		if ( $target = get_campaign_meta( $campaign_id, 'target_blank', true ) )
			update_post_meta( $post_id, 'post_target', $target );
	}

	/**
	 * Format some post attributes before post creation
	 * @param  arrary $postarr Post Data Array
	 * @param  object $item    SimplePie Feed Item object
	 * @return array           Filtered post data
	 */
	public function format_import_post_data( $postarr, $item ) {
		// Run decode twice becaseu SimplePie may encode entities for a string that already has encoded entities
		if ( !empty( $postarr['post_title'] ) )
			$postarr['post_title'] = html_entity_decode( html_entity_decode( $postarr['post_title'] ) );

		return $postarr;
	}

	/**
	 * Allow to import some restricted by wordpress html tags
	 * @param  array $allowedposttags  Currently allowed html tags
	 * @param  strgin $context         Current kses context, we are altering only post context
	 * @return array                   Filtered allowed html tags
	 */
	public function allow_additional_html( $allowedposttags, $context ) {
		if ( 'post' != $context )
			return $allowedposttags;

		$allowedposttags['iframe'] = array(
			'src'             => array(),
			'height'          => array(),
			'width'           => array(),
			'frameborder'     => array(),
			'allowfullscreen' => array(),
		);

		$allowedposttags['script'] = array(
			'src'     => array(),
			'type'    => array(),
			'charset' => array(),
			'async'   => array()
		);

		return $allowedposttags;
	}
}
new RSSI_Campaign_Import;
