<?php
/**
 * Public methods for campaign model
 * Including campaign code by id retrieval and campaign meta functions
 */

/**
 * Get Campaign post_type key
 * @return string
 */
function rssi_get_campaign_post_type() {
	global $rssi_campaign;

	return $rssi_campaign->post_type;
}

/**
 * Remove metadata matching criteria from a banner.
 *
 * You can match based on the key, or key and value. Removing based on key and
 * value, will keep from removing duplicate metadata with the same key. It also
 * allows removing all metadata matching key, if needed.
 *
 * @since 1.0
 * @uses $wpdb
 *
 * @param int $post_id post ID
 * @param string $meta_key Metadata name.
 * @param mixed $meta_value Optional. Metadata value.
 * @return bool False for failure. True for success.
 */
function delete_campaign_meta( $post_id, $meta_key, $meta_value = '' ) {
	// make sure meta is added to the post, not a revision
	if ( $the_post = wp_is_post_revision($post_id) )
		$post_id = $the_post;

	return delete_metadata('campaign', $post_id, $meta_key, $meta_value);
}

/**
 * Retrieve post meta field for a campaign.
 *
 * @since 1.0
 * @uses $wpdb
 *
 * @param int $post_id Post ID.
 * @param string $key Optional. The meta key to retrieve. By default, returns data for all keys.
 * @param bool $single Whether to return a single value.
 * @return mixed Will be an array if $single is false. Will be value of meta data field if $single
 *  is true.
 */
function get_campaign_meta( $post_id, $key = '', $single = false ) {
	return get_metadata( 'campaign', $post_id, $key, $single );
}

/**
 * Update post meta field based on campaign post ID.
 *
 * Use the $prev_value parameter to differentiate between meta fields with the
 * same key and post ID.
 *
 * If the meta field for the post does not exist, it will be added.
 *
 * @since 1.5.0
 * @uses $wpdb
 *
 * @param int $post_id Post ID.
 * @param string $meta_key Metadata key.
 * @param mixed $meta_value Metadata value.
 * @param mixed $prev_value Optional. Previous value to check before removing.
 * @return bool False on failure, true if success.
 */
function update_campaign_meta( $post_id, $meta_key, $meta_value, $prev_value = '' ) {
	// make sure meta is added to the post, not a revision
	if ( $the_post = wp_is_post_revision($post_id) )
		$post_id = $the_post;

	return update_metadata( 'campaign', $post_id, $meta_key, $meta_value, $prev_value );
}

