<?php
/**
 * Register advertise post type
 * each post is single advertisement
 */

add_action( 'init', 'create_campaign_post_type' );

function create_campaign_post_type() {

	register_post_type( rssi_get_campaign_post_type(),
		array(
			'labels' => array(
				'name' => __( 'WPB Ingestor', 'adv' ),
				'singular_name' => __( 'WPB Ingestor ', 'adv' ),
				'all_items' => __( 'All Ingestor Feeds', 'adv' ),
				'add_new' => __( 'Add Ingestor Feed', 'adv' ),
				'add_new_item' => __( 'Add New Ingestor Feed', 'adv' ),
				'edit' => __( 'Edit', 'adv' ),
				'edit_item' => __( 'Edit Ingestor Feed', 'adv' ),
				'new_item' => __( 'New Ingestor Feed', 'adv' ),
				'view' => __( 'View Ingestor Feed', 'adv' ),
				'view_item' => __( 'View Ingestor Feed', 'adv' ),
				'search_items' => __( 'Search Ingestor Feed', 'adv' ),
				'not_found' => __( 'No Ingestor Feed found', 'adv' ),
				'not_found_in_trash' => __( 'No Ingestor Feeds found in Trash', 'adv' ),
			),
			'public' => true,
			'exclude_from_search' => true,
			'publicly_queryable' => false,

			'menu_position' => 32,
			'hierarchical' => false,
			'query_var' => true,
			'supports' => array( 'title', 'custom-fields' ),
			'rewrite' => false,
			'can_export' => true,
		)
	);
};