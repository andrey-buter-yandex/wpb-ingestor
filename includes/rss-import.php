<?php
/**
* Main Rss Import plugin class
* Initialize options and broacast loaded action for sub modules
*
* 
*/
class Rss_Import {

	/**
	 * Holds options object
	 *
	 * @since 2.0
	 * @access public
	 * @var object
	 */
	var $options;

	/**
	 * Holds default plugin options
	 *
	 * @since 2.0
	 * @access public
	 * @var array
	 */
	var $default_options = array(
		'initial_nag' => 1
	);

	/**
	 * PHP5 constructor
	 *
	 * @since 2.0
	 * @access public
	 */
	function __construct() {

		// Init before load action
		do_action( 'rssi_before_setup' );

		// Initialize plugin options
		$this->init_options();

		// Make plugin object accessible via global object
		$GLOBALS['rssi'] = $this;

		// Tell submodules that plugin main class loaded
		do_action( 'rssi_after_setup' );
	}

	/**
	 * Initializes plugin options object
	 * set default options
	 *
	 * @since 2.0
	 * @access public
	 */
	function init_options() {
		$this->options = new RSSI_Plugin_Options( 'rssi', apply_filters( 'rssi_init_options', $this->default_options ) );
	}

	/**
	 * Uninstalls Advertise Plugin
	 *
	 * @since 2.0
	 * @access private
	 */
	function uninstall() {

		require_once( ABSPATH . 'wp-admin/includes/plugin.php' );

		// Delete options
		delete_option( 'adv' );
	}

	/**
	 * Install Advertise Plugin
	 *
	 * @since 2.0
	 * @access private
	 */
	function install() {
		do_action( 'adv_install' );
	}

	/**
	 * PHP4 style constructor
	 *
	 * @since 2.0
	 * @access public
	 */
	function ADVertise() {
		$this->__construct();
	}
}