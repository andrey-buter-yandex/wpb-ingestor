<?php
/*
Plugin Name: WPB Ingestor
Description: Import Feeds directly from external rss feed
Author: WPB
Version: 1.2
Author URI: http://wordpressforbroadcasters.com/
License: GPLv2 or later
Text Domain: rssi
Domain Path: /i18n
*/

/**
 * Current plugin version
 * Each time on plugin initialization 
 * we are checking this version with one stored in plugin options
 * and if they don't match plugin update hook will be fired
 *
 * @since 2.1
 */
if ( !defined( 'RSSI_VERSION' ) )
	define( 'RSSI_VERSION', '1.0' );

/**
 * The name of the SocialFlow Core file
 *
 * @since 2.0
 */
if ( !defined( 'RSSI_FILE' ) )
	define( 'RSSI_FILE', __FILE__ );

/**
 * Absolute location of SocialFlow Plugin
 *
 * @since 2.0
 */
if ( !defined( 'RSSI_ABSPATH' ) )
	define( 'RSSI_ABSPATH', dirname( RSSI_FILE ) );

/**
 * The name of the SocialFlow directory
 *
 * @since 2.0
 */
if ( !defined( 'RSSI_DIRNAME' ) )
	define( 'RSSI_DIRNAME', basename( RSSI_ABSPATH ) );

/**
 * Enable debug mode
 *
 * @since 2.0
 */
if ( !defined( 'RSSI_DEBUG' ) )
	define( 'RSSI_DEBUG', false );

// Require plugin essential files
// All of them are top level inside includes
// inner dependencies are loaded by this main classes

// require_once( RSSI_ABSPATH . '/modules/debuger/debuger.php' );

require_once( RSSI_ABSPATH . '/includes/shared.php' );
require_once( RSSI_ABSPATH . '/includes/campaign.php' );
require_once( RSSI_ABSPATH . '/includes/feed.php' );

// Include modules
require_once( RSSI_ABSPATH . '/modules/map-taxonomy/map-taxonomy.php' );
require_once( RSSI_ABSPATH . '/modules/map-xml/map-xml.php' );
require_once( RSSI_ABSPATH . '/modules/filter-duplicate/filter-duplicate.php' );
require_once( RSSI_ABSPATH . '/modules/podcast/podcast.php' );

require_once( RSSI_ABSPATH . '/modules/map-expiry/map-expiry.php' );

// Include ramp parser manually 
require_once( RSSI_ABSPATH . '/modules/ramp-parser/ramp-parser.php' );

require_once( RSSI_ABSPATH . '/includes/rss-import.php' );

/**
 * Rss Import main object initializtion
 * is globally accessible via $rssi variable
 * @global object $adv
 * @since 2.0
 */
new Rss_Import();