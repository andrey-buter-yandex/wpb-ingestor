=== WPB Ingestor ===
Contributors: WPB
Requires at least: 3.2
Tested up to: 3.9.1
Stable tag: 1.2

Import Posts from external rss feed directly into your WordPress.


== Description ==
WPB Ingestor allowes you to grab posts from external feed and import them into your WordPress website.
You can specify author, post status, discusion status or even apply categories to imported posts.
There is also support for itunes feed and integration with WPB episodes.
Custom XML Mapping allows you to map your feed attributes with your post attributes.

You can also turn on RAMP support,
To enable RAMP parser you must define RSSI_ENABLE_RAMP constant in your functions.php for example like this:
<?php define( RSSI_ENABLE_RAMP, true ); ?>

==Installation==
Simply activate the plugin and add new Ingestor feed feeling in all the options and hit Publish.

== Frequently Asked Questions ==
No questions yet.


== Changelog == 

= 2.2 =
* Define RSSI_ENABLE_RAMP inside theme to enable RAMP

= 2.1 =
* Minor bug fixes

= 2.0 =
* Plugin rewritten from scratch.

= 1.0 =
Initial Release.

Some text